<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TutorType extends Model
{
    protected $table = "m_tutor_type";
}
