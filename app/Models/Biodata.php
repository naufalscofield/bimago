<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TutorType;
use App\Models\Location;

class Biodata extends Model
{
    protected $table    = "biodata";

    public function tipe_bimbel()
    {
        return $this->hasOne(TutorType::class, 'id', 'id_tutor_type');
    }
    public function location()
    {
        return $this->hasOne(Location::class, 'id', 'id_learn_place');
    }
}
