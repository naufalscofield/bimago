<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Biodata;
use App\Models\TutorType;

class PaymentSpp extends Model
{
    protected $table = 'payment_spp';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id_user');
    }
}
