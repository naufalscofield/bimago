<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipeBimbel extends Model
{
    use SoftDeletes;

    protected $table    = 'm_tutor_type';
    protected $dates    = ['deleted_at'];
}
