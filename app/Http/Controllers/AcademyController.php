<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;
use App\Models\Spp;

class AcademyController extends Controller
{
    public function sso()
    {
        $spp    = Spp::where(['id_user' => Auth::user()->id, 'year' => date('Y'), 'month' => date('m')])->first();
        if ($spp->status == 4)
        {
            $stat_spp = "sudah";
        } else
        {
            $stat_spp = "belum";
        }
        $nama   = $this->split_name(Auth::user()->name);
        $user   = [
            'email'         => Auth::user()->email,
            'no_hp'         => Auth::user()->biodata->parent_phone,
            'nama_depan'    => $nama[0],
            'nama_belakang' => $nama[1],
            'api_key'       => 'Qs6DvJ5w8B9CZgrXfgcTfzqJqmYr5Orj',
            'stat_spp'      => $stat_spp
        ];

        $response = Curl::to('https://bimago.academy/sso')
        ->withData( $user )
        ->asJson()
        ->post();
        
        return Redirect::to($response->redirect_link);
    }

    public function redirect()
    {
        return Redirect::to("https://bimago.academy");
    }

    public function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.preg_quote($last_name,'#').'#', '', $name ) );
        return array($first_name, $last_name);
    }
}
