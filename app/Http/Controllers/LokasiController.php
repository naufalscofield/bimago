<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
// use App\Models\Regional;
use Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\Auth;

class LokasiController extends Controller
{
    public function index()
    {
        // $data['regional'] = Regional::all();
        return view('admin.location.index');
    }

    public function get(Request $request)
    {
        // $offset = $request->start;
        // $limit  = $request->length;

        // $lokasi         = Location::limit($limit)
        //                             ->offset($offset)
        //                             ->get();

        $lokasi = Location::get();

        return Datatables::of($lokasi)
        ->addIndexColumn()
        ->addColumn('alamat_lengkap', function($row){
            return "Kecamatan ".$row->kecamatan.' | '.$row->address;
        })
        ->addColumn('regional', function($row){
            return $row->provinsi.' - '.$row->regional;
        })
        ->addColumn('action', function($row){
            $btn = '<button title="Delete" class="btn btn-danger btn-delete btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-trash"></i>&nbsp
                    </button><button title="Edit / Detail" class="btn btn-info btn-edit btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-pencil"></i>
                    </button>';
            return $btn;
        })
        ->rawColumns(['alamat_lengkap','action', 'regional'])
        // ->setTotalRecords($lokasiTotal->count())
        // ->setFilteredRecords($lokasi->count())
        ->make(true);
    }

    public function show($id)
    {
        $id = decrypt($id);

        $lokasi  = Location::find($id);
        $lokasi->enc_id = encrypt($lokasi->id);

        return response()->json($lokasi, 200);
    }

    public function store(Request $request)
    {
        $rules  = [
            'name'          => 'required|string',
            'name_provinsi' => 'required|string',
            'name_regional' => 'required|string',
            'name_kecamatan'=> 'required|string',
            'address'       => 'required|string',
            'description'   => 'required|string'
        ];

        $messages = [
            'name.required'             => 'Nama lokasi harus diisi',
            'name_provinsi.required'    => 'Provinsi lokasi harus diisi',
            'name_regional.required'    => 'Regional lokasi harus diisi',
            'name_kecamatan.required'   => 'Kecamatan lokasi harus diisi',
            'address.required'          => 'Alamat lengkap harus diisi',
            'address.string'            => 'Alamat lengkap harus berupa karakter',
            'description.required'      => 'Deskripsi lengkap harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $lokasi             = new Location;
        $lokasi->name       = $request->name;
        $lokasi->provinsi   = $request->name_provinsi;
        $lokasi->regional   = $request->name_regional;
        $lokasi->kecamatan  = $request->name_kecamatan;
        $lokasi->address    = $request->address;
        $lokasi->description= $request->description;
        $lokasi->created_by = Auth::user()->id;
        
        try {
            $lokasi->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menyimpan data lokasi");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.lokasi");
        }
        
        toastr()->success('Lokasi baru berhasil ditambahkan');
        return redirect()->route('admin.lokasi');
    }

    public function delete($id)
    {
        $id = decrypt($id);

        $lokasi     = Location::find($id);

        try {
            $lokasi->delete();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menghapus data lokasi");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.lokasi");
        }

        toastr()->success('Lokasi berhasil dihapus');
        return response()->json($lokasi, 200);
    }

    public function update(Request $request)
    {
        $rules  = [
            'e_id'            => 'required|string',
            'e_name'          => 'required|string',
            'e_regional'      => 'required|string',
            // 'e_kecamatan'     => 'required|string',
            'e_address'       => 'required|string',
            'e_description'   => 'required|string'
        ];

        $messages = [
            'e_id.required'               => 'Id lokasi harus diisi',
            'e_name.required'             => 'Nama lokasi harus diisi',
            'e_regional.required'         => 'Regional lokasi harus diisi',
            // 'e_kecamatan.required'        => 'Kecamatan harus diisi',
            // 'e_kecamatan.string'          => 'kecamatan harus berupa karakter',
            'e_address.required'          => 'Alamat lengkap harus diisi',
            'e_address.string'            => 'Alamat lengkap harus berupa karakter',
            'e_description.required'      => 'Deskripsi lengkap harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $id     = decrypt($request->e_id);
        
        $lokasi             = Location::find($id);
        $lokasi->name       = $request->e_name;
        $lokasi->regional   = $request->e_regional;
        $lokasi->kecamatan  = ($request->e_kecamatan != "") ? $request->e_kecamatan : "-";
        $lokasi->address    = $request->e_address;
        $lokasi->description= $request->e_description;
        $lokasi->updated_by = Auth::user()->id;

        try {
            $lokasi->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat memperbarui data lokasi");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.lokasi");
        }

        toastr()->success('Lokasi berhasil diperbarui');
        return redirect()->route("admin.lokasi");
    }

    public function indexUser($regional)
    {
        $regional           = str_replace("%20", " ", $regional);
        if ($regional == 'all')
        {
            $data['lokasi'] = Location::all();
        } else
        {
            $data['lokasi'] = Location::where('regional', $regional)->get();
        }
        
        $data['curr_reg']   = $regional;
        $data['reg']        = Location::select('regional')
                                    ->groupBy('regional')
                                    ->get();
        // dd($data['lokasi']);
        return view('home.lokasi', $data);
    }
}
