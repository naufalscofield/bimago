<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Spp;
use App\Models\Rekening;
use App\Models\TutorType;
use App\Models\PaymentSpp;
use App\Models\Biodata;
use App\Models\Notification;
use App\Models\Location;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use DB;
use Validator;
use Datatables;
use Session;
use App\Http\Helpers\InfoHelper;

class SppController extends Controller
{
    public function index()
    {
        $data['spp']    = Spp::where('id_user', Auth::user()->id)
                        ->where('year', date('Y'))
                        ->where('month', '<=', date('m'))
                        ->where('status', 0)
                        ->get();
        $biodata        = Biodata::where('id_user', Auth::user()->id)->first();
        $data['bimbel'] = TutorType::find($biodata->id_tutor_type)->first();
        return view('spp.index', $data);
    }

    public function amount($id, $spp_1, $spp_2)
    {
        try {
            $spp_1 = decrypt($spp_1);
        } catch (\Exception $e)
        {
            return response()->view('errors.403');
        }

        try {
            $spp_2 = decrypt($spp_2);
        } catch (\Exception $e)
        {
            return response()->view('errors.403');
        }

        $from   = Spp::find($spp_1);
        $to     = Spp::find($spp_2);

        $total_bulan = ($to->month - $from->month) + 1;

        $biodata        = Biodata::where('id_user', $id)->first();
        $bimbel         = TutorType::find($biodata->id_tutor_type)->first();

        $biaya_bulanan  = $bimbel->price;

        $amount         = $biaya_bulanan * $total_bulan;

        $res            = [
            'amount'        => "Rp " . number_format($amount,2,',','.'),
            'from_to'       => 'Pembayaran bimbel '.strtoupper($bimbel->type).' dari bulan ke '.$from->month.' - bulan ke '.$to->month.' ('.$total_bulan.' Bulan)',
        ];

        return response()->json($res, 200);
    }

    public function pay($id, $spp_1, $spp_2)
    {
        try {
            $spp_1 = decrypt($spp_1);
        } catch (\Exception $e)
        {
            return response()->view('errors.403');
        }

        try {
            $spp_2 = decrypt($spp_2);
        } catch (\Exception $e)
        {
            return response()->view('errors.403');
        }

        $from   = Spp::find($spp_1);
        $to     = Spp::find($spp_2);

        $total_bulan = ($to->month - $from->month) + 1;

        $biodata        = Biodata::where('id_user', $id)->first();
        $bimbel         = TutorType::find($biodata->id_tutor_type)->first();

        $biaya_bulanan  = $bimbel->price;

        $amount         = $biaya_bulanan * $total_bulan;

        $data_payment   = [
            'id_user'       => $id,
            'amount'        => $amount,
            'year'          => date('Y'),
            'from_month'    => $from->month,
            'to_month'      => $to->month,
            'status'        => 0,
            'receipt'       => NULL,
            'verified_by'   => NULL,
            'created_at'    => date('Y-m-d H:i:s')
        ];

        try {
            PaymentSpp::insert($data_payment);
        } catch (\Exception $e)
        {
            toastr()->error('Terjadi kesalahan saat menyimpan data pembayaran');
            toastr()->error($e->getMessage());
        }

        for ($i=$from->month; $i<=$to->month; $i++)
        {
            $spp         = Spp::where(['year' => date('Y'), 'month' => $i, 'id_user' => $id])->first();
            $spp->status = 1;

            $spp->save();
        }

        toastr()->success('Pembayaran berhasil dibuat, silahkan lakukan pelunasan dan upload bukti pembayaran');
        return redirect()->route('spp.riwayat');
    }

    public function riwayat()
    {
        $biodata            = Biodata::where('id_user', Auth::user()->id)->first();
        $data['bimbel']     = TutorType::find($biodata->id_tutor_type)->first();
        $data['riwayat']    = PaymentSpp::where('id_user', Auth::user()->id)->get();
        // dd($data['riwayat']);
        $data['rekening']   = Rekening::all()->first();

        return view('spp.riwayat', $data);
    }

    public function upload(Request $request)
    {
        $rules  = [
            'id_payment'      => 'required',
            'receipt'         => 'required',
        ];

        $messages = [
            'id_payment.required'        => 'ID Payment harus diisi',
            'receipt.required'           => 'Receipt harus diupload',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $id_payment     = decrypt($request->id_payment);

        $file           = $request->file('receipt');
        $file_name      = $id_payment.'_'.date('d-m-Y').'.'.$file->getClientOriginalExtension();

        $upload_path    = public_path() . '/assets/spp_receipt'; 

        try {
            $file->move($upload_path, $file_name);
        } catch (\Exception $e)
        {
            toastr()->error('Terjadi kesalahan saat upload file');
            toastr()->error($e->getMessage());
            return redirect()->back();
        }

        $payment         = PaymentSpp::find($id_payment);
        $payment->status = 1;
        $payment->receipt= $file_name;

        try {
            $payment->save();
        } catch (\Exception $e)
        {
            toastr()->error('Terjadi kesalahan saat update status pembayaran');
            toastr()->error($e->getMessage());
            return redirect()->back();
        }

        for ($i=$payment->from_month; $i<=$payment->to_month; $i++)
        {
            $spp         = Spp::where(['year' => date('Y'), 'month' => $i, 'id_user' => Auth::user()->id])->first();
            $spp->status = 2;

            try {
                $spp->save();
            } catch (\Exception $e)
            {
                toastr()->error('Terjadi kesalahan saat update status spp');
                toastr()->error($e->getMessage());
                return redirect()->back();
            }
        }

        $notif  = new Notification;
        $notif->title   = 'Pembayaran SPP baru telah masuk, harap dicek!';
        $notif->url     = route('admin.spp');
        $notif->status  = 0;
        $notif->id_item = $payment->id;

        $notif->save();

        toastr()->success('Upload bukti pembayaran berhasil, harap menunggu verifikasi');
        return redirect()->route('spp.riwayat');
    }

    public function indexAdmin()
    {
        return view('admin.spp.index');
    }

    public function show($id)
    {
        $id = decrypt($id);

        $payment  = PaymentSpp::where('id', $id)
                        ->with('user.biodata.tipe_bimbel')
                        ->with('user.biodata')
                        ->with('user.biodata.tipe_bimbel')
                        ->first();
        $payment->total_amount  = "Rp " . number_format($payment->amount,2,',','.');
        $payment->periode       = 'Tahun '.$payment->year.' : Bulan ke '.$payment->from_month.' sampai bulan ke '.$payment->to_month;
        $payment->bulan         = ($payment->to_month - $payment->from_month) + 1 . ' Bulan';
        $payment->receipt_path  = asset('public/assets/spp_receipt/'.$payment->receipt);
        $payment->id_payment    = encrypt($id);

        return response()->json($payment, 200);
    }

    public function get(Request $request)
    {
        // $offset = $request->start;
        // $limit  = $request->length;

        $payment  = PaymentSpp::with('user.biodata.tipe_bimbel')
                        ->with('user.biodata')
                        ->with('user.biodata.tipe_bimbel')
                        ->with('user.biodata.location')
                        // ->limit($limit)
                        // ->offset($offset)
                        ->get();
        if ($payment->count() != 0)
        {
            return Datatables::of($payment)
            ->addIndexColumn()
            ->addColumn('nama', function($row){
                return $row['user']->name;
            })
            ->addColumn('location', function($row){
                return $row['user']['biodata']['location']->name;
            })
            ->addColumn('tipe', function($row){
                return $row['user']['biodata']['tipe_bimbel']->type;
            })
            ->addColumn('total', function($row){
                return "Rp " . number_format($row->amount,2,',','.');
            })
            ->addColumn('periode', function($row){
                return 'Tahun '.$row->year.' : Bulan ke '.$row->from_month.' sampai bulan ke '.$row->to_month;
            })
            ->addColumn('bulan', function($row){
                return ($row->to_month - $row->from_month) + 1 . ' Bulan';
            })
            ->addColumn('action', function($row){
                if ($row->status == 1)
                {
                    $btn = '<button title="Verifikasi" style="width:100%" class="btn btn-success btn-verifikasi btn-sm" data-id="'.encrypt($row->id).'">
                                <i class="fa fa-check"></i> Verifikasi&nbsp
                            </button>
                            <div class="blink_me">
                                <h5><span style="width:100%" class="badge badge-danger">Belum Diverifikasi</span></h5>
                            </div>';
                } else if ($row->status == 2)
                {
                    $btn = '<button title="Verifikasi Manual" style="width:100%" class="btn btn-info btn-verifikasi-manual btn-sm" data-id="'.encrypt($row->id).'">
                                <i class="fa fa-repeat"></i> Ubah Status Verifikasi Diterima Manual&nbsp
                            </button>
                            <div class="">
                                <h5><span style="width:100%" class="badge badge-danger">Sudah Diverifikasi Ditolak</span></h5>
                            </div>';
                } else if ($row->status == 3)
                {
                    $btn = "<h5><span class='badge badge-success'>Sudah Diverifikasi Diterima</span></h5>";
                } else
                {
                    $btn = "<h5><span class='badge badge-danger'>Belum dibayar</span></h5>";
                }
    
                return $btn;
            })
            ->rawColumns(['nama','tipe','total','periode','bulan','action','location'])
            ->make(true);

        }
    }

    public function verifikasi(Request $request)
    {
        $rules  = [
            'id'            => 'required',
            'keterangan'    => 'required',
            'status'        => 'required',
        ];

        $messages = [
            'id.required'           => 'ID Payment harus diisi',
            'keterangan.required'   => 'Keterangan harus diisi',
            'status.required'       => 'Status harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        try {
            $id = decrypt($request->id);
        } catch (\Exception $e)
        {
            return response()->view('errors.403');
        }

        $payment                = PaymentSpp::find($id);
        $payment->keterangan    = $request->keterangan;
        $payment->status        = $request->status;

        try {
            $payment->save();
        } catch (\Exception $e)
        {
            toastr()->error($e->getMessage());
            toastr()->error('Terjadi kesalahan saat memverifikasi pembayaran');
            return redirect()->back();
        }

        $data_siswa     = User::where('id',$payment->id_user)
                                ->with('biodata')
                                ->first();

        $data_location  = Location::find($data_siswa['biodata']->id_learn_place)->first();

        if ($request->status == 3)
        {
            for ($i=$payment->from_month; $i<=$payment->to_month; $i++)
            {
                $spp         = Spp::where(['year' => $payment->year, 'month' => $i, 'id_user' => $payment->id_user])->first();
                $spp->status = 4;
    
                $spp->save();

            }
            //Send Email
            $details    = [
                'title' => 'Verifikasi Pembayaran Bimago',
                'body1' => 'Assalamualaikum '.$data_siswa->name.', Pembayaran anda telah diverifikasi dan DITERIMA, anda dapat mulai belajar pada jadwal selanjutnya di lokasi Bimago yang telah anda pilih.',
                'body2' => 'Lokasi : '.$data_location->name.', '.$data_location->address.', '.$data_location->kecamatan.', '.$data_location->regional.', '.$data_location->provinsi.', ',
                'body3' => 'Jadwal & CP : '.$data_location->description,
                'base'  => 'https://santri.bimago.academy'
            ];
    
            try {
                \Mail::to($data_siswa->email)->send(new \App\Mail\NotificationEmail($details));
            } catch (\Exception $e)
            {
            }
            //
        } else
        {
            for ($i=$payment->from_month; $i<=$payment->to_month; $i++)
            {
                $spp         = Spp::where(['year' => $payment->year, 'month' => $i, 'id_user' => $payment->id_user])->first();
                $spp->status = 3;
    
                $spp->save();
                
            }
            //Send Email
            $details    = [
                'title' => 'Verifikasi Pembayaran Bimago',
                'body1' => 'Assalamualaikum '.$data_siswa->name.', Pembayaran anda telah diverifikasi dan DITOLAK.',
                'body2' => 'Keterangan : '.$payment->keterangan,
                'body3' => 'Info lebih lanjut dapat menghubungi nomor '.InfoHelper::getInfo()->phone,
                'base'  => 'https://santri.bimago.academy'
            ];
    
            try {
                \Mail::to($data_siswa->email)->send(new \App\Mail\NotificationEmail($details));
            } catch (\Exception $e)
            {
            }
            //
        }

        toastr()->success('Pembayaran berhasil diverifikasi');
        return redirect()->route('admin.spp');
    }

    public function verifikasiManual($id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e)
        {
            return response()->json('forbidden', 403);
        }

        $payment                = PaymentSpp::find($id);
        $payment->keterangan    = '-';
        $payment->status        = 3;

        try {
            $payment->save();
        } catch (\Exception $e)
        {
            return response()->json('failed', 400);
        }

        for ($i=$payment->from_month; $i<=$payment->to_month; $i++)
        {
            $spp         = Spp::where(['year' => $payment->year, 'month' => $i, 'id_user' => $payment->id_user])->first();
            $spp->status = 4;

            $spp->save();
        }

        $data_siswa     = User::where('id',$payment->id_user)
                                ->with('biodata')
                                ->first();

        $data_location  = Location::find($data_siswa['biodata']->id_learn_place)->first();

        //Send Email
        $details    = [
            'title' => 'Verifikasi Pembayaran Bimago',
            'body1' => 'Assalamualaikum '.$data_siswa->name.', Pembayaran anda telah diverifikasi dan DITERIMA, anda dapat mulai belajar pada jadwal selanjutnya di lokasi Bimago yang telah anda pilih.',
            'body2' => 'Lokasi : '.$data_location->name.', '.$data_location->address.', '.$data_location->kecamatan.', '.$data_location->regional.', '.$data_location->provinsi.', ',
            'body3' => 'Jadwal & CP : '.$data_location->description,
            'base'  => 'https://santri.bimago.academy'
        ];

        try {
            \Mail::to($data_siswa->email)->send(new \App\Mail\NotificationEmail($details));
        } catch (\Exception $e)
        {
        }
        //

        return response()->json('success', 200);
    }

    public function forbidden()
    {
        return response()->view('errors.403');
    }

}   
