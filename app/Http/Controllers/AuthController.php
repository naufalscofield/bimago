<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use Hash;
use Session;
use Mail;
use App\User;
use App\Models\Biodata;
use App\Models\TutorType;
use App\Models\Location;
use App\Models\Spp;
use App\Models\PaymentSpp;
use Ixudra\Curl\Facades\Curl;

class AuthController extends Controller
{
    public function login()
    {
        return view("auth.login");
    }

    public function own()
    {
        return view("auth.own");
    }

    public function checkLogin(Request $request)
    {
        $rules  = [
            'email'     => 'required|email',
            'password'  => 'required|string'
        ];

        $messages = [
            'email.required'        => 'Email harus diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password harus diisi',
            'password.string'       => 'Password harus berupa karakter'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }

        $cred = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
        
        Auth::attempt($cred);

        if (Auth::check()) { 
            if (Auth::user()->email_verified_at == NULL) 
            {
                toastr()->error("Akun anda belum verifikasi email");
                return redirect()->back()->withInput($request->all());
            } else
            {
                if (Auth::user()->role == "santri")
                {
                    Session::put('is_login', true);
                    toastr()->success("Selamat datang");
                    return redirect()->route('home');
                } else
                {
                    toastr()->error("Akun tidak ditemukan");
                    return redirect()->back()->withInput($request->all());
                }
            }
        } else {
            toastr()->error("Akun tidak ditemukan");
            return redirect()->back()->withInput($request->all());
        }
    }

    public function checkOwn(Request $request)
    {
        $rules  = [
            'email'     => 'required|email',
            'password'  => 'required|string'
        ];

        $messages = [
            'email.required'        => 'Email harus diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password harus diisi',
            'password.string'       => 'Password harus berupa karakter'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }

        $cred = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
        
        Auth::attempt($cred);

        if (Auth::check()) { 
            if (Auth::user()->role == "admin")
            {
                toastr()->success("Selamat datang");
                return redirect()->route('admin.dashboard');
            } else
            {
                toastr()->error("Akun tidak ditemukan");
                return redirect()->back()->withInput($request->all());
            }
        } else {
            toastr()->error("Akun tidak ditemukan");
            return redirect()->back()->withInput($request->all());
        }
    }

    public function register()
    {
        $data           = [
            'tutor_type'    => TutorType::all(),
            'learn_place'   => Location::all()
        ];

        return view("auth.register", $data);
    }

    public function actRegister(Request $request)
    {
        $rules  = [
            'name'     => 'required|string',
            'provinsi'     => 'required',
            'regional'     => 'required',
            'kecamatan'     => 'required',
            'address'     => 'required',
            'email'     => 'required|email',
            'password'  => 'required|string',
            'birthplace'  => 'required',
            'birthdate'  => 'required',
            'school'  => 'required',
            'graduated'  => 'required',
            'parent_name'  => 'required',
            'parent_phone'  => 'required|min:11|max:13',
            'id_tutor_type'  => 'required',
            'id_learn_place'  => 'required',
        ];

        $messages = [
            'name.string'           => 'Nama harus berupa karakter non angka',
            'provinsi.required'     => 'Asal provinsi harus diisi',
            'regional.required'     => 'Asal regional harus diisi',
            'kecamatan.required'    => 'Kecamatan harus diisi',
            'address.required'      => 'Alamat lengkap harus diisi',
            'email.required'        => 'Email harus diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password harus diisi',
            'password.string'       => 'Password harus berupa karakter',
            'birthplace.required'   => 'Tempat lahir harus diisi',
            'birthdate.required'    => 'Tanggal lahir harus diisi',
            'school.required'       => 'Asal sekolah harus diisi',
            'graduated.required'    => 'Lulusan harus diisi',
            'parent_name.required'  => 'Nama orang tua harus diisi',
            'parent_phone.required' => 'No telp orang tua harus diisi',
            'parent_phone.min'      => 'No telp orang tua minimal 11 angka',
            'parent_phone.max'      => 'No telp orang tua maksimal 13 angka',
            'id_tutor_type.required'=> 'Tipe bimbel harus diisi',
            'id_learn_place.required'=> 'Lokasi tempat belajar harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }

        $user = new User;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = Hash::make($request->password);
        $user->role         = "santri";
        $user->remember_token = Str::random(16);

        try {
            $user->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menyimpan data");
            return redirect()->route("auth.register");
        }

        $biodata = new Biodata;
        $biodata->id_user       = $user->id;
        $biodata->regional      = $request->provinsi."-".$request->regional;
        $biodata->kecamatan     = $request->kecamatan;
        $biodata->address       = $request->address;
        $biodata->birthplace    = $request->birthplace;
        $biodata->birthdate     = $request->birthdate;
        $biodata->school        = $request->school;
        $biodata->graduated     = $request->graduated;
        $biodata->parent_name   = $request->parent_name;
        $biodata->parent_phone  = $request->parent_phone;
        $biodata->id_tutor_type = $request->id_tutor_type;
        $biodata->id_learn_place = ($request->id_tutor_type == 1) ? $request->id_learn_place : NULL;
        $biodata->request_day   = ($request->id_tutor_type == 2) ? $request->request_day : NULL;
        $biodata->request_time  = ($request->id_tutor_type == 2) ? $request->request_time : NULL;

        try {
            $biodata->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menyimpan data detail");
            return redirect()->route("auth.register");
        }

        $arr_spp    = [];

        $sup_spp    = [
            'id_user'           => $user->id,
            'id_tipe_bimbel'    => $request->id_tutor_type,
            'year'              => date('Y'),
            'month'             => 1,
            'status'            => 0,
        ];

        array_push($arr_spp, $sup_spp);

        Spp::insert($arr_spp);


        //Payment
        $total_bulan    = 1;

        $biodata        = Biodata::where('id_user', $user->id)->first();
        $bimbel         = TutorType::find($biodata->id_tutor_type)->first();

        $biaya_bulanan  = $bimbel->price;

        $amount         = $biaya_bulanan * $total_bulan;

        $data_payment   = [
            'id_user'       => $user->id,
            'amount'        => $amount,
            'year'          => date('Y'),
            'from_month'    => 1,
            'to_month'      => 1,
            'status'        => 0,
            'receipt'       => NULL,
            'verified_by'   => NULL,
            'created_at'    => date('Y-m-d H:i:s')
        ];

        try {
            PaymentSpp::insert($data_payment);
        } catch (\Exception $e)
        {
            toastr()->error('Terjadi kesalahan saat menyimpan data pembayaran');
            toastr()->error($e->getMessage());
        }

        $spp         = Spp::where(['year' => date('Y'), 'month' => 1, 'id_user' => $user->id])->first();
        $spp->status = 1;

        $spp->save();
        //

        return redirect('/send-email-verification/'.encrypt($user->id));
    }

    public function sendEmail($id)
    {
        $id         = decrypt($id);
        $user       = User::find($id);
        
        if ($user != NULL)
        {
            $details    = [
                'title' => 'Email verifikasi akun '.strtoupper(env("APP_NAME")),
                'body' => 'Terimakasih telah mendaftar, berikut link verifikasi akun anda',
                'user' => $user,
                'link' => asset('/verify-email').'/'.$user->remember_token,
                'base' => public_path()
            ];
    
            try {
                \Mail::to($user->email)->send(new \App\Mail\VerificationEmail($details));
            }  catch (Throwable $e) {
                toastr()->error($e->getMessage());
                toastr()->error('Pendaftaran akun gagal karna kesalahan sistem');
                return redirect()->back();
            }
            toastr()->success('Pendaftaran akun berhasil, silahkan cek email anda untuk verifikasi akun anda');

            return redirect()->route("auth.login");

        }
    }

    public function verifyEmail($token)
    {
        $user = User::where("remember_token", $token)->first();

        if ($user != NULL)
        {
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();

            toastr()->success('Akun anda berhasil diverifikasi');
            return redirect()->route('auth.login');
        } else
        {
            return view("errors.404");
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('auth.login');
    } 

    public function split_name($name) 
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.preg_quote($last_name,'#').'#', '', $name ) );
        return array($first_name, $last_name);
    }
}
