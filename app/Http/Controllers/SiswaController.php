<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Datatables;
use Illuminate\Support\Facades\Crypt;

class SiswaController extends Controller
{
    public function index()
    {
        return view('admin.siswa.index');
    }

    public function get(Request $request)
    {
        // $offset = $request->start;
        // $limit  = $request->length;

        $siswa  = User::where('role', 'santri')
                        ->with('payment')
                        ->with('biodata')
                        ->with('biodata.location')
                        ->whereHas('payment', function ($query) {
                            return $query->where('status', '=', 3);
                        })
                        // ->where('payment_spp.status', 3)
                        // ->limit($limit)
                        // ->offset($offset)
                        ->get();

        return Datatables::of($siswa)
        ->addIndexColumn()
        ->addColumn('nama', function($row){
            return $row->name;
        })
        ->addColumn('location', function($row){
            return $row['biodata']['location']->name;
        })
        ->addColumn('email', function($row){
            return $row->email;
        })
        ->addColumn('tanggal_daftar', function($row){
            return date("d F Y", strtotime($row->created_at));
        })
        ->addColumn('lulusan', function($row){
            return $row['biodata']->graduated;
        })
        ->addColumn('no_telp_org_tua', function($row){
            return $row['biodata']->parent_phone;
        })
        ->addColumn('action', function($row){
            $btn = '<button title="Detail" class="btn btn-info btn-detail btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-pencil"></i>
                    </button>';
            return $btn;
        })
        ->rawColumns(['nama', 'email', 'lulusan', 'tanggal_daftar', 'no_telp_org_tua', 'action', 'location'])
        ->make(true);
    }

    public function show($id)
    {
        $id = decrypt($id);

        $siswa  = User::where('id',$id)
                        ->with('biodata')
                        ->with('biodata.location')
                        ->first();
        $siswa->tanggal_daftar  = date("d F Y", strtotime($siswa->created_at));
        $siswa->ttl             = $siswa['biodata']->birthplace.', '.date("d F Y", strtotime($siswa['biodata']->birthdate));
        return response()->json($siswa, 200);
    }
}
