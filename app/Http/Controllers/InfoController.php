<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Info;
use Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;

class InfoController extends Controller
{
    public function index()
    {
        $data['info']  = Info::where('id',1)->first();
        return view('admin.info.index', $data);
    }

    public function update(Request $request)
    {
        $rules  = [
            'about'         => 'required',
            'address'       => 'required',
            'email'         => 'required|email',
            'phone'         => 'required|numeric',
        ];

        $messages = [
            'about.required'            => "Deskripsi 'Tentang' harus diisi",
            'address.required'          => "Alamat harus diisi",
            'email.required'            => "Email harus diisi",
            'email.email'               => "Email tidak valid",
            'phone.required'            => "No telp harus diisi",
            'phone.number'              => "No telp tidak valid, harus berupa angka",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $info             = Info::find(1);
        $info->about      = $request->about;
        $info->address    = $request->address;
        $info->email      = $request->email;
        $info->phone      = $request->phone;
        $info->link_gmaps = $request->link_gmaps;

        try {
            $info->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat memperbarui data info");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.info");
        }
        
        toastr()->success('Data info berhasil diperbarui');
        return redirect()->route('admin.info');
    }
    
}
