<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Regional;
use Illuminate\Support\Facades\Crypt;
use Datatables;
use Validator;

class RegionalController extends Controller
{
    public function index()
    {
        return view('admin.regional.index');
    }

    public function get()
    {
        $regional   = Regional::all();

        return Datatables::of($regional)
        ->addIndexColumn()
        ->addColumn('action', function($row){
            $btn = '<button title="Delete" class="btn btn-danger btn-delete btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-trash"></i>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function store(Request $request)
    {
        $rules  = [
            'provinsi'      => 'required|string',
            'regional'      => 'required|string',
            'name_regional' => 'required|string',
            'name_provinsi' => 'required|string',
        ];

        $messages = [
            'provinsi.required'         => 'Provinsi harus diisi',
            'regional.required'         => 'Regional harus diisi',
            'name_regional.required'    => 'Nama Regional harus diisi',
            'name_provinsi.required'    => 'Nama Provinsi harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $regional = new Regional;
        $regional->provinsi     = $request->name_provinsi;
        $regional->regional_code= $request->regional;
        $regional->regional     = $request->name_regional;

        try {
            $regional->save();
        } catch (\Exception $e)
        {
            toastr()->error($e->getMessage());
            return redirect()->back();
        }

        return redirect()->route('admin.regional');
    }

    public function delete($id)
    {
        $id = decrypt($id);

        $regional     = Regional::find($id);

        try {
            $regional->delete();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menghapus data regional");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.regional");
        }

        toastr()->success('Regional berhasil dihapus');
        return response()->json($regional, 200);
    }
}
