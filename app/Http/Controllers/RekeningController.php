<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rekening;
use Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;

class RekeningController extends Controller
{
    public function index()
    {
        $data['rekening']  = Rekening::where('id',1)->first();
        return view('admin.rekening.index', $data);
    }

    public function update(Request $request)
    {
        $rules  = [
            'bank'          => 'required|string',
            'name'          => 'required|string',
            'number'        => 'required|numeric',
        ];

        $messages = [
            'bank.required'             => 'Nama Bank harus diisi',
            'bank.string'               => 'Nama Bank harus berupa karakter non angka',
            'name.required'             => 'Atas Nama harus diisi',
            'name.string'               => 'Atas Nama harus berupa karakter non angka',
            'number.required'           => 'No Rekening harus diisi',
            'number.string'             => 'No Rekening harus berupa angka',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $rekening             = Rekening::find(1);
        $rekening->bank       = $request->bank;
        $rekening->name       = $request->name;
        $rekening->number     = $request->number;
        
        try {
            $rekening->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat memperbarui data rekening");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.rekening");
        }
        
        toastr()->success('Data rekening berhasil diperbarui');
        return redirect()->route('admin.rekening');
    }
    
}
