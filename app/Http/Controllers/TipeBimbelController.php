<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipeBimbel;
use Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\Auth;

class TipeBimbelController extends Controller
{
    public function index()
    {
        return view('admin.tipe_bimbel.index');
    }

    public function get(Request $request)
    {
        // $offset = $request->start;
        // $limit  = $request->length;

        $bimbel  = TipeBimbel::get();
        // $bimbel  = TipeBimbel::limit($limit)
        //                 ->offset($offset)
        //                 ->get();

        return Datatables::of($bimbel)
        ->addIndexColumn()
        ->addColumn('harga', function($row){
            return "Rp " . number_format($row->price,2,',','.');
        })
        ->addColumn('action', function($row){
            $btn = '<button title="Delete" class="btn btn-danger btn-delete btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-trash"></i>&nbsp
                    </button><button title="Edit / Detail" class="btn btn-info btn-edit btn-sm" data-id="'.encrypt($row->id).'">
                        <i class="fa fa-pencil"></i>
                    </button>';
            return $btn;
        })
        ->rawColumns(['harga','action'])
        ->make(true);
    }

    public function show($id)
    {
        $id = decrypt($id);

        $bimbel  = TipeBimbel::find($id);
        $bimbel->enc_id = encrypt($bimbel->id);

        return response()->json($bimbel, 200);
    }

    public function store(Request $request)
    {
        $rules  = [
            'type'          => 'required|string',
            'description'   => 'required|string',
            'price'         => 'required|numeric'
        ];

        $messages = [
            'type.required'             => 'Tipe harus diisi',
            'description.required'      => 'Deskripsi harus diisi',
            'price.string'              => 'Harga harus diisi',
            'price.numeric'              => 'Harga harus berupa angka'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $bimbel             = new TipeBimbel;
        $bimbel->type       = $request->type;
        $bimbel->description  = $request->description;
        $bimbel->price      = $request->price;
        
        try {
            $bimbel->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menyimpan data tipe bimbel");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.tipe.bimbel");
        }
        
        toastr()->success('Tipe bimbel baru berhasil ditambahkan');
        return redirect()->route('admin.tipe.bimbel');
    }

    public function delete($id)
    {
        $id = decrypt($id);

        $bimbel     = TipeBimbel::find($id);

        try {
            $bimbel->delete();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menghapus data tipe bimbel");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.tipe.bimbel");
        }

        toastr()->success('Tipe bimbel berhasil dihapus');
        return response()->json($bimbel, 200);
    }

    public function update(Request $request)
    {
        $rules  = [
            'e_id'            => 'required|string',
            'e_type'          => 'required|string',
            'e_description'   => 'required|string',
            'e_price'         => 'required|numeric'
        ];

        $messages = [
            'e_id.required'               => 'Id tipe harus diisi',
            'e_type.required'             => 'Tipe harus diisi',
            'e_description.required'      => 'Deskripsi harus diisi',
            'e_price.required'            => 'Harga lengkap harus diisi',
            'e_price.numeric'              => 'Haega harus berupa angka'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $id     = decrypt($request->e_id);
        
        $lokasi             = TipeBimbel::find($id);
        $lokasi->type       = $request->e_type;
        $lokasi->description= $request->e_description;
        $lokasi->price      = $request->e_price;

        try {
            $lokasi->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat memperbarui data tipe bimbel");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.tipe.bimbel");
        }

        toastr()->success('Tipe bimbel berhasil diperbarui');
        return redirect()->route("admin.tipe.bimbel");
    }
}
