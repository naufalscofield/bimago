<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin.index');
    }

    public function get(Request $request)
    {
        // $offset = $request->start;
        // $limit  = $request->length;

        // $admin  = User::where('role','admin')
        //                 ->limit($limit)
        //                 ->offset($offset)
        //                 ->get();
        $admin  = User::where('role','admin')
                        ->get();

        return Datatables::of($admin)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $request)
    {
        $rules  = [
            'name'          => 'required|string',
            'email'         => 'required|email',
            'password'      => 'required',
        ];

        $messages = [
            'name.required'             => 'Nama harus diisi',
            'name.string'               => 'Nama harus berupa karakter non angka',
            'email.required'            => 'Email harus diisi',
            'email.email'               => 'Email tidak valid',
            'password.required'         => 'Password harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $admin             = new User;
        $admin->name       = $request->name;
        $admin->email      = $request->email;
        $admin->password   = Hash::make($request->password);
        $admin->role       = 'admin';
        $admin->email_verified_at   = date("Y-m-d");
        $admin->remember_token      = NULL;
        $admin->created_at   = date("Y-m-d");
        $admin->updated_at   = date("Y-m-d");
        
        try {
            $admin->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat menyimpan admin baru");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.admin");
        }
        
        toastr()->success('Admin baru berhasil ditambahkan');
        return redirect()->route('admin.admin');
    }

    public function update(Request $request)
    {
        $rules  = [
            'name'            => 'required|string',
            'email'           => 'required|email',
        ];

        $messages = [
            'name.required'              => 'Name harus diisi',
            'name.string'                => 'Name harus berupa karakter non numeric',
            'email.required'             => 'Email harus diisi',
            'email.email'                => 'Email tidak valid',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $id     = Auth::user()->id;
        
        $user             = User::find($id);
        $user->name       = $request->name;
        $user->email      = $request->email;

        try {
            $user->save();
        } catch (Throwable $e) {
            toastr()->error("Terjadi kesalahan saat memperbarui data profil");
            toastr()->error($e->getMessage());
            return redirect()->route("admin.account");
        }

        toastr()->success('Profil berhasil diperbarui');
        return redirect()->route("admin.account");
    }

    public function account()
    {
        $user           = User::find(Auth::user()->id);

        $data['user']   = $user;
        return view('admin.account.index', $data);
        
    }

    public function changePassword(Request $request)
    {
        $rules  = [
            'new_password'    => 'required',
            'old_password'    => 'required'
        ];

        $messages = [
            'new_password.required'     => 'Password baru harus diisi',
            'old_password.email'        => 'Password lama harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            $error  = $validator->messages()->get('*');
            foreach ($error as $e)
            {
                foreach ($e as $e)
                {
                    toastr()->error($e);
                }
            }
            return redirect()->back()->withInput($request->all());
        }  

        $old_password   = Hash::make($request->old_password);
        $new_password   = Hash::make($request->new_password);

        $cred = [
            'email'     => Auth::user()->email,
            'password'  => $request->old_password,
        ];
        
        if (Auth::attempt($cred))
        {
                $user   = User::find(Auth::user()->id);
                $user->password = $new_password;
    
                try {
                    $user->save();
                } catch (Throwable $e) {
                    toastr()->error("Terjadi kesalahan saat memperbarui password");
                    toastr()->error($e->getMessage());
                    return redirect()->back();
                }
    
                toastr()->success('Password berhasil diperbarui');
                return redirect()->route('admin.account');
        } else
        {
            toastr()->error('Password lama tidak sesuai');
            return redirect()->back();
        }
    }
    
}
