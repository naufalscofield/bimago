<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Ixudra\Curl\Facades\Curl;
use App\Models\Location;

class HomeController extends Controller
{
    public function index()
    {
        return view("home.index");
    }

    // public function getKecamatan()
    // {
    //     $get_token = Curl::to('https://x.rajaapi.com/poe')
    //     ->get();
    //     $token  = json_decode($get_token);
        
    //     if ($token != NULL)
    //     {
    //         $token  = $token->token;
    //         // $token = "BObcM6Vk0LPUZETge3aIdZvaEYHVhDzsSw4HaK7QZ90TuG5u47";
            
    //         $arr_kecamatan  = [];
    
    //         $get_kecamatan1 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3273')
    //         ->get();
    //         $data_kecamatan1    = json_decode($get_kecamatan1);
    //         array_push($arr_kecamatan, $data_kecamatan1->data);
    
    //         $get_kecamatan2 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3204')
    //         ->get();
    //         $data_kecamatan2    = json_decode($get_kecamatan2);
    //         array_push($arr_kecamatan, $data_kecamatan2->data);
    
    //         $get_kecamatan3 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3217')
    //         ->get();
    //         $data_kecamatan3    = json_decode($get_kecamatan3);
    //         array_push($arr_kecamatan, $data_kecamatan3->data);
    
    //         $get_kecamatan4 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3277')
    //         ->get();
    //         $data_kecamatan4    = json_decode($get_kecamatan4);
    //         array_push($arr_kecamatan, $data_kecamatan4->data);
    
    //         return response()->json($arr_kecamatan, 200);
    //     } else
    //     {
    //         return response()->json('null', 200);
    //     }
    // }

    public function showKecamatan($regional)
    {
        $get_token = Curl::to('https://x.rajaapi.com/poe')
        ->get();
        $token  = json_decode($get_token);
        
        if ($token != NULL)
        {
            $token  = $token->token;
            
            switch($regional)
            {
                case 1:
                    $get_kecamatan1 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3273')
                    ->get();
                    $data_kecamatan1    = json_decode($get_kecamatan1);
                    return response()->json($data_kecamatan1, 200);
                    break;
                case 2:
                    $get_kecamatan2 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3204')
                    ->get();
                    $data_kecamatan2    = json_decode($get_kecamatan2);
                    return response()->json($data_kecamatan2, 200);
                    break;
                case 3:
                    $get_kecamatan3 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3217')
                    ->get();
                    $data_kecamatan3    = json_decode($get_kecamatan3);
                    return response()->json($data_kecamatan3, 200);
                    break;
                case 4:
                    $get_kecamatan4 = Curl::to('https://x.rajaapi.com/MeP7c5ne'.$token.'/m/wilayah/kecamatan?idkabupaten=3277')
                    ->get();
                    $data_kecamatan4    = json_decode($get_kecamatan4);
                    return response()->json($data_kecamatan4, 200);
                    break;
            }
        } else
        {
            return response()->json('null', 200);
        }
    }

    public function showLokasi($provinsi, $regional, $kecamatan)
    {
        $provinsi  = str_replace("_", " ", $provinsi);
        $provinsi  = strtoupper($provinsi);

        $regional  = str_replace("_", " ", $regional);
        $regional  = strtoupper($regional);

        $kecamatan  = str_replace("_", " ", $kecamatan);
        $kecamatan  = strtoupper($kecamatan);

        $lokasi     = Location::where("kecamatan", $kecamatan)->first();

        if ($lokasi != NULL)
        {
            return response()->json(['stat' => 1, 'data' => $lokasi], 200);
        } else
        {
            $lokasi2   = Location::where('regional', $regional)->get();
            // dd($lokasi2);
            if ($lokasi2->count() != 0)
            {
                return response()->json(['stat' => 2, 'data' => $lokasi2], 200);
            } else
            {
                $lokasi3   = Location::where('provinsi', $provinsi)->get();
                if ($lokasi3->count() != 0)
                {
                    return response()->json(['stat' => 3, 'data' => $lokasi3], 200);
                } else
                {
                    return response()->json(['stat' => 0], 200);
                }
            }
        }
    }
}
