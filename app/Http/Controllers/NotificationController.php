<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use Session;

class NotificationController extends Controller
{
    public function get()
    {
        $notif  = Notification::where('status', 0)->get();
        $res    = [
            'notif' => $notif,
            'total' => count($notif)
        ];

        return response()->json($res,200);
    }

    public function update($id)
    {
        $update = Notification::find($id);
        $update->status = 1;
        $update->save();

        if ($update->id_item != NULL)
        {
            Session::flash('notification', encrypt($update->id_item));
        }
        return response()->json('success', 200);
    }
}
