<?php

namespace App\Http\Helpers;
use App\Models\Info;
use App\User;
use App\Models\Location;
use App\Models\PaymentSpp;

class InfoHelper
{
    public static function getInfo()
    {
        $info   = Info::where('id', 1)->first();

        return $info;
    }

    public static function resizeMap($html)
    {
        $dom = new \DOMDocument;
        $dom->loadHTML($html);
        $iframes = $dom->getElementsByTagName('iframe');
        if (isset($iframes[0])) {
            $iframes[0]->setAttribute('width', '100%');
            return $dom->saveHTML($iframes[0]);
        } else {
            return false;
        }
    }

    public static function dashboard()
    {
        $data['siswa']  = User::where('role','santri')
                                ->with('payment')
                                ->whereHas('payment', function ($query) {
                                    return $query->where('status', '=', 3);
                                })
                                ->count();
        $data['cabang'] = Location::count();
        $data['pending']= PaymentSpp::where('status',1)->count();
        $data['register']= User::whereYear('created_at', '=', date('Y'))
                                ->whereMonth('created_at', '=', date('m'))
                                ->count();

        return $data;
    }

    public static function location()
    {
        $regional   = Location::select('regional')
                                ->groupBy('regional')
                                ->get();
        $arr= [];

        foreach($regional as $r)
        {
            $location   = Location::where('regional', $r->regional)
                                    ->get();

            array_push($arr, $location);
        }

        return $arr;
    }
}