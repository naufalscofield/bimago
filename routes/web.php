<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name("home");

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return redirect()->back();
});

Route::get('/login', 'AuthController@login')->name("auth.login");
Route::post('/login-check', 'AuthController@checkLogin')->name("auth.login.check");
Route::get('/register', 'AuthController@register')->name("auth.register");
Route::post('/register-act', 'AuthController@actRegister')->name("auth.register.act");
Route::get('/get-kecamatan', 'HomeController@getKecamatan')->name("data.kecamatan");
Route::get('/show-lokasi/{provinsi}/{regional}/{kecamatan}', 'HomeController@showLokasi')->name("data.lokasi.show");
Route::get('/show-kecamatan/{regional}', 'HomeController@showKecamatan')->name("data.kecamatan.show");
Route::get('/own', 'AuthController@own')->name("auth.own");
Route::post('/own-check', 'AuthController@checkOwn')->name("auth.own.check");
Route::get('/send-email-verification/{id}', 'AuthController@sendEmail')->name('auth.send.email');
Route::get('/verify-email/{token}', 'AuthController@verifyEmail')->name('auth.verify.email');
Route::get('/academy-redirect', 'AcademyController@redirect')->name("academy.redirect");
Route::get('/lokasi/{regional}', 'LokasiController@indexUser')->name("lokasi");

Route::group(['middleware' => ['auth', 'role:santri,ustadz,admin']], function () {
    Route::get('/logout', 'AuthController@logout')->name("auth.logout");
});

Route::group(['middleware' => ['auth', 'role:santri']], function () {
    Route::get('/academy-sso', 'AcademyController@sso')->name("academy.sso");
    Route::get('/spp', 'SppController@index')->name("spp");
    Route::get('/spp/amount/{id}/{spp_1}/{spp_2}', 'SppController@amount')->name("spp.amount");
    Route::get('/spp/pay/{id}/{spp_1}/{spp_2}', 'SppController@pay')->name("spp.pay");
    Route::get('/spp/riwayat', 'SppController@riwayat')->name("spp.riwayat");
    Route::post('/spp/upload', 'SppController@upload')->name("spp.upload");
});

Route::group(['middleware' => ['auth', 'role:admin']], function () {
    Route::get('/admin/dashboard', 'DashboardController@index')->name("admin.dashboard");

    Route::get('/admin/siswa', 'SiswaController@index')->name("admin.siswa");
    Route::get('/admin/siswa/get', 'SiswaController@get')->name("admin.siswa.get");
    Route::get('/admin/siswa/show/{id}', 'SiswaController@show')->name("admin.siswa.show");

    Route::get('/admin/lokasi', 'LokasiController@index')->name("admin.lokasi");
    Route::get('/admin/lokasi/get', 'LokasiController@get')->name("admin.lokasi.get");
    Route::get('/admin/lokasi/show/{id}', 'LokasiController@show')->name("admin.lokasi.show");
    Route::post('/admin/lokasi/store', 'LokasiController@store')->name("admin.lokasi.store");
    Route::post('/admin/lokasi/update', 'LokasiController@update')->name("admin.lokasi.update");
    Route::get('/admin/lokasi/delete/{id}', 'LokasiController@delete')->name("admin.lokasi.delete");

    Route::get('/admin/tipe-bimbel', 'TipeBimbelController@index')->name("admin.tipe.bimbel");
    Route::get('/admin/tipe-bimbel/get', 'TipeBimbelController@get')->name("admin.tipe.bimbel.get");
    Route::get('/admin/tipe-bimbel/show/{id}', 'TipeBimbelController@show')->name("admin.tipe.bimbel.show");
    Route::post('/admin/tipe-bimbel/store', 'TipeBimbelController@store')->name("admin.tipe.bimbel.store");
    Route::post('/admin/tipe-bimbel/update', 'TipeBimbelController@update')->name("admin.tipe.bimbel.update");
    Route::get('/admin/tipe-bimbel/delete/{id}', 'TipeBimbelController@delete')->name("admin.tipe.bimbel.delete");

    Route::get('/admin/admin', 'AdminController@index')->name("admin.admin");
    Route::get('/admin/admin/get', 'AdminController@get')->name("admin.admin.get");
    Route::get('/admin/admin/show/{id}', 'AdminController@show')->name("admin.admin.show");
    Route::post('/admin/admin/store', 'AdminController@store')->name("admin.admin.store");
    Route::post('/admin/admin/update', 'AdminController@update')->name("admin.admin.update");
    Route::get('/admin/admin/delete/{id}', 'AdminController@delete')->name("admin.admin.delete");
    Route::get('/admin/admin/account', 'AdminController@account')->name("admin.account");
    Route::post('/admin/admin/change-password', 'AdminController@changePassword')->name("admin.account.change.password");

    Route::get('/admin/spp', 'SppController@indexAdmin')->name("admin.spp");
    Route::get('/admin/spp/get', 'SppController@get')->name("admin.spp.get");
    Route::get('/admin/spp/show/{id}', 'SppController@show')->name("admin.spp.show");
    Route::post('/admin/spp/verifikasi', 'SppController@verifikasi')->name("admin.spp.verifikasi");
    Route::get('/admin/spp/verifikasi-manual/{id}', 'SppController@verifikasiManual')->name("admin.spp.verifikasi.manual");
    Route::get('/admin/spp/403', 'SppController@forbidden')->name("admin.spp.403");

    Route::get('/admin/rekening', 'RekeningController@index')->name("admin.rekening");
    Route::get('/admin/rekening/get', 'RekeningController@get')->name("admin.rekening.get");
    Route::post('/admin/rekening/update', 'RekeningController@update')->name("admin.rekening.update");

    Route::get('/admin/info', 'InfoController@index')->name("admin.info");
    Route::get('/admin/info/get', 'InfoController@get')->name("admin.info.get");
    Route::post('/admin/info/update', 'InfoController@update')->name("admin.info.update");
    
    Route::get('/admin/regional', 'RegionalController@index')->name("admin.regional");
    Route::get('/admin/regional/get', 'RegionalController@get')->name("admin.regional.get");
    Route::post('/admin/regional/store', 'RegionalController@store')->name("admin.regional.store");
    Route::get('/admin/regional/delete/{id}', 'RegionalController@delete')->name("admin.regional.delete");

    Route::get('/admin/notification/get', 'NotificationController@get')->name("admin.notification.get");
    Route::get('/admin/notification/update/{id}', 'NotificationController@update')->name("admin.notification.update");

});
