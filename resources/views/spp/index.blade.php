@extends('template', ['menu' => 'spp'])

@section('content')
    
  <!-- event section -->
  <section class="event_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h3>
          SPP Bulanan
        </h3>
        <p>
            (Tipe bimbel : {{ $bimbel->type }})
        </p>
      </div>
      <br>
      <label for="">Bayar SPP Untuk/Dari Bulan Ke :</label>
      <select name="spp_1" id="spp_1" class="form-control">
          <option selected value="{{ encrypt($spp[0]->id) }}">Bulan ke {{ $spp[0]->month }}</option>
      </select>
      <br>
      <label for="">Sampai Bulan Ke :</label>
      <select name="spp_2" id="spp_2" class="form-control">
          <option value="">Pilih</option>
            @if (count($spp) > 1 )
                @for ($i = 1; $i < count($spp); $i++)
                    <option value="{{ encrypt($spp[$i]->id)}}">Bulan ke {{ $spp[$i]->month }}</option>
                @endfor
            @else
                <option value="{{ encrypt($spp[0]->id) }}">Bulan ke {{ $spp[0]->month }}</option>
            @endif
      </select>
      <br>
      <center>
        <div class="spinner-border text-warning" id="spinner" style="display:none" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </center>
      <div class="event_container" id="div_detail" style="display:none">
        <div class="box">
          <div class="img-box">
            <img src="{{ asset('') }}public/bootstrap/joson/images/event-img.jpg" alt="" />
          </div>
          <div class="detail-box text-center">
            <h4 id="amount">
            </h4>
            <h6 style="size:70%;margin:5px" id="total_bulan">

            </h6>
            <label style="color:red;font-size:10px" id="" for="">* Pembayaran bulan-bulan selanjutnya, dibayarkan langsung ke tempat Bimago masing masing.</label>
          </div>
          <div class="date-box">
              <a class="btn btn-primary" id="btn_bayar" style="background-color:#DAA520 !important;border-color:#DAA520 !important;font-size:10px"><h3 style="color:white">Bayar</h3></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
      $(document).on('change', '#spp_2', function(){
          $("#spinner").show()
          if ($(this).val() !== "")
          {
            var spp_1 = $("#spp_1").val()
            var spp_2 = $(this).val()
            var usr_id    = "{{ Auth::user()->id }}"
            var route = "{{ route('spp.amount', [":id", ":spp_1", ":spp_2"]) }}"
            route     = route.replace(":id", usr_id)
            route     = route.replace(":spp_1", spp_1)
            route     = route.replace(":spp_2", spp_2)

            var route2 = "{{ route('spp.pay', [":id", ":spp_1", ":spp_2"]) }}"
            route2     = route2.replace(":id", usr_id)
            route2     = route2.replace(":spp_1", spp_1)
            route2     = route2.replace(":spp_2", spp_2)

            $.get(route, function(data, status, xhr){
                if (xhr.status == 200)
                {
                    $("#spinner").hide()
                    $("#div_detail").show()
                    $("#amount").text(data['amount'])
                    // $("#total_bulan").text(data['from_to'])
                    $("#total_bulan").text("Pembayaran biaya pendaftaran dan biaya SPP bulan pertama")
                    $("#btn_bayar").attr("href", route2)
                } else
                {
                    toastr.error('Gagal mendapatkan detail jumlah pembayaran')
                }
            })
          }
      })
  </script>

  <!-- end event section -->
@endsection