@extends('template', ['menu' => 'riwayat_spp'])

@section('content')
  <style>
    .blink_me {
      animation: blinker 1s linear infinite;
    }

    @keyframes blinker {
      50% {
        opacity: 0;
      }
    }
  </style>
  <!-- event section -->
  <section class="event_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h3>
          Riwayat Pembayaran SPP
        </h3>
        <p>
            (Tipe bimbel : {{ $bimbel->type }})
        </p>
      </div>
      <br>
      <div class="event_container" id="">
        @foreach ($riwayat as $r)
          <div class="box">
            <div class="img-box">
              <img src="{{ asset('') }}public/bootstrap/joson/images/event-img.jpg" alt="" />
            </div>
            <div class="detail-box text-center">
              <h4 id="amount">
                {{ "Rp. " . number_format($r->amount,2,',','.') }}
              </h4>
              <h6 style="size:70%" id="total_bulan">
                Tahun {{ $r->year }} Bulan ke {{ $r->from_month }} sampai Bulan ke {{ $r->to_month }} & Biaya Pendaftaran
                <br>
                @switch($r->status)
                    @case(0)
                    <div class="blink_me">
                        <h3><span class="badge badge-danger">Belum Bayar</span></h3>
                    </div>
                        @break
                    @case(1)
                        <h3><span class="badge badge-info">Dalam Verifikasi</span></h3>
                        @break
                    @case(2)
                        <h3><span class="badge badge-warning">Selesai | Pembayaran Ditolak</span></h3>
                        <span style="color:#DAA520">
                          Keterangan : {{ $r->keterangan }}
                        </span>
                        @break
                    @case(3)
                        <h3><span class="badge badge-success">Selesai | Pembayaran Diterima</span></h3>
                        @break
                    @default
                @endswitch
              </h6>
            </div>
            <div class="date-box">
              @switch($r->status)
                  @case(0)
                      <div class="row">
                        <button class="btn btn-primary btn-upload" data-id="{{ encrypt($r->id) }}" data-target="#modal_upload" data-toggle="modal" style="background-color:#DAA520 !important;border-color:#DAA520 !important">Upload Bukti</button> &nbsp
                        <button class="btn btn-info btn-cara" data-id="{{ encrypt($r->id) }}" data-target="#modal_cara" data-toggle="modal" style="">Cara Pembayaran</button>
                      </div>
                      @break
                  @case(1)
                      <button class="btn btn-info btn-receipt" data-id="{{ encrypt($r->id) }}" data-receipt="{{ asset('public/assets/spp_receipt/').'/'.$r->receipt }}" data-target="#modal_receipt" data-toggle="modal">Lihat Bukti</button>
                      @break
                  @case(2)
                      <button class="btn btn-info btn-receipt" data-id="{{ encrypt($r->id) }}" data-receipt="{{ asset('public/assets/spp_receipt/').'/'.$r->receipt }}" data-target="#modal_receipt" data-toggle="modal">Lihat Bukti</button>
                      @break
                  @case(3)
                      <button class="btn btn-info btn-receipt" data-id="{{ encrypt($r->id) }}" data-receipt="{{ asset('public/assets/spp_receipt/').'/'.$r->receipt }}" data-target="#modal_receipt" data-toggle="modal">Lihat Bukti</button>
                      @break
                  @default
              @endswitch
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('spp.upload') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="id_payment" value="{{ encrypt($riwayat[0]->id) }}" class="id_payment" name="id_payment">
            <input type="file" class="form-control" required name="receipt">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_cara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cara Pembayaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>
            Lakukan pembayaran menggunakan metode transfer melalui ATM / M-Banking ke rekening berikut:
          </p>
          <table class="table table-responsive-lg">
            <tr>
              <th>Bank</th>
              <td>: {{ $rekening->bank }}</td>
            </tr>
            <tr>
              <th>Atas Nama</th>
              <td>: {{ $rekening->name }}</td>
            </tr>
            <tr>
              <th>No Rekening</th>
              <td>: {{ $rekening->number }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_receipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bukti Pembayaran Anda</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <img src="{{ asset('public/assets/spp_receipt/').'/'.$riwayat[0]->receipt }}" id="receipt" style="width:50%;height:50%" alt="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script>
      // function upload()
      // {
      //   $(".id_payment").val()
      // }
      // $(document).on('click', '.btn-receipt', function(){
      //   $("#receipt").attr("src", $(this).data("receipt"))
      // })
      
      })
  </script>

  <!-- end event section -->
@endsection