@extends('template', ['menu' => 'lokasi'])

@section('content')
<section class="event_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h3>
          Lokasi Bimago
        </h3>
        <p>
          Lokasi Bimago tersebar dibeberapa titik
        </p>
        <select name="" id="lokasi" class="form-control" id="">
            <option {{ ($curr_reg == 'all') ? 'selected' : '' }} value="all">Semua Regional</option>
            @foreach ($reg as $r)
                <option {{ ($r->regional == $curr_reg) ? 'selected' : '' }} value="{{ $r->regional }}">{{ $r->regional }}</option>
            @endforeach
            {{-- <option {{ ($reg == 2) ? 'selected' : '' }} value="2">Kabupaten Bandung</option>
            <option {{ ($reg == 3) ? 'selected' : '' }} value="3">Kabupaten Bandung Barat</option>
            <option {{ ($reg == 4) ? 'selected' : '' }} value="4">Kota Cimahi</option> --}}
        </select>
      </div>
      <div class="event_container">
          <br>
        <label style="color:#DAA520" for="">Ditemukan {{ count($lokasi) }} Lokasi</label>
        @foreach ($lokasi as $l)
            <div class="box">
                <div class="img-box text-center">
                    <p>
                        {{ $l->description }}
                    </p>
                </div>
                <div class="detail-box text-center">
                    <h4>
                    {{ $l->name }}
                    </h4>
                    <label style="margin-left:10px;margin-right:10px">
                    {{ $l->kecamatan.' | '.$l->address }}
                    </label>
                </div>
                <div class="date-box text-center">
                    <h3>
                    {{ $l->regional }}
                    </h3>
                </div>
            </div>
        @endforeach
      </div>
    </div>
  </section>    

  <script>
      $(document).on('change', '#lokasi', function(){
          var val   = $(this).val()
          var route = "{{ route('lokasi', ":regional") }}"
          route = route.replace(":regional", val)

          window.location.href = route;
      })
  </script>
@endsection