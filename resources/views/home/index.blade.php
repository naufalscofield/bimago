@inject('info_helper', 'App\Http\Helpers\InfoHelper')
@extends('template', ['menu' => 'home'])


    @section('content')
    <style>
      @media (max-width: 480px) {
        .banner_title1 {
          font-size: 20px !important;
        }
        .banner_title2 {
          font-size: 30px !important;
        }
        .banner_title3 {
          font-size: 10px !important;
        }
      }

		@media (max-width: 768px) {
      .banner_title1 {
        font-size: 20px !important;
      }
      .banner_title2 {
        font-size: 30px !important;
      }
      .banner_title3 {
        font-size: 10px !important;
      }

      .div-materi {
        flex-basis: 50% !important;
      }

      .div-text-materi {
        flex-basis: 50% !important;
      }

      .div-blue-box {
        flex-basis: 40% !important;
      }

      .detail-blue-box {
        font-size: 100% !important;
      }
      .detail-blue-box2 {
        font-size: 70% !important;
      }
      #img_bimago_academy {
        width: 30% !important;
      }
      #div_gallery_1 {
        padding: 0px 100px !important;
      }
      #div_gallery_2 {
        padding: 0px 85px !important;
      }

    }
    </style>
    <!-- slider section -->
  	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet'>
  	<link href='https://fonts.googleapis.com/css?family=MS Reference Sans Serif' rel='stylesheet'>
    <section class=" slider_section position-relative">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container">
              <div class="m-t-0">
                <img class="img-responsive" src="{{ asset('') }}public/assets/img/bimago-bdg.png" style="width:auto" alt="">
                <br>
                <h1 class="banner_title1" style="color:#000;font-size:50px;font-family:MS Reference Sans Serif;display:inline">Menyiapkan</h1>
										<span class="banner_title2" style="color:#000;font-size:60px;font-family:Pacifico">Calon Santri!</span>
										<br>
										<span class="banner_title3" style="font-size:15px;color:#000">Solusi belajar untuk calon santri wilayah Bandung dan sekitarnya<br>untuk mempersiapkan diri menghadapi ujian masuk Gontor</span>
                <span>
                  <br>
                  @if (!Auth::check())
                    <a style="font-size:12px;width: 150px;margin-top:38px;color:#fff;background-color:#DAA520;border-color:#DAA520;border-radius:25px" class="btn btn-primary" href="{{ route('auth.register' ) }}">Daftar Sekarang</a>
                  @endif
                </span>
              </div>
              <div class="row">
                <div class="col">
                  <div class="detail-box">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <section class="about_section layout_padding" id="about">
    <div class="side_img">
      <img src="{{ asset('') }}public/bootstrap/joson/images/side-img.png" alt="" />
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-center">
            <img src="{{ asset('') }}public/assets/img/ikpm.png" style="width:30%" alt="" />
            <img src="{{ asset('') }}public/assets/img/bimago-bdg.png"style="width:30%"  alt="" />
        </div>
        <div class="col-md-6">
          <div class="detail-box">
            <div class="heading_container">
              <h3>
                BIMAGO Bandung Raya
              </h3>
              <p style="text-align:justify">
                  BIMAGO atau
                  Bimbingan Masuk Gontor adalah 
                  bimbingan belalar khusus untuk
                  persiapan ujian
                  masuk Pondok Pesantren Modern Darussalam Gontor.
                  <br>
                  <br>
                  Bimbel ini merupakan salah satu program Divisi Dakwah dan Pendidikan Ikatan
                  Keluarga Pondok Modern (IKPM) Gontor Cabang Bandung Raya yang diadakan di
                  wilayah Bandung Raya dan seluruh pengajarnya adalah para alumni Pondok modern
                  DARUSSALAM GONTOR.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->

  <!-- course section -->

  <section class="course_section layout_padding-bottom" style="background-color:#edf4f7" id="kursus">
    <div class="side_img">
      <img src="{{ asset('') }}public/bootstrap/joson/images/side-img.png" alt="" />
    </div>
    <div class="container">
      <div class="heading_container">
        <br>
        <h3>
          Materi Bimbingan
        </h3>
      </div>
      <div class="course_container">
        <div class="course_content">
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/icon-quran.png" style="width:85px;height:85px" alt="" />
            <p>AL QUR'AN</p>
            <p style="font-size:80%">Tahsin & Tajwid</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/icon-imla.png" style="width:85px;height:85px" alt="" />
            <p>IMLA</p>
            <p style="font-size:80%">Belajar menulis tulisan arab yang baik & benar</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/icon-fiqih.png" style="width:85px;height:85px" alt="" />
            <p>FIQIH</p>
            <p style="font-size:80%">Ibadah Qouliyah & Ibadah Amaliyah</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/icon-berhitung.png" style="width:85px;height:85px" alt="" />
            <p>BERHITUNG</p>
            {{-- <p style="font-size:80%">Belajar menulis tulisan arab yang baik & benar</p> --}}
          </div>
        </div>
      </div>
      <hr style="height:1px;border:none;color:#333;background-color:#333;">
      <br>
      <div class="course_container">
        <div class="course_content">
          <div class="text-center div-text-materi" style="flex-basis: 30%">
            <h4 style="font-weight:bold">Waktu Bimbingan</h4>
            <br>
            <label style="font-size:80%">2x pertemuan dalam seminggu</label><br>
            <label style="font-size:80%">Sabtu & Minggu</label><br>
            <label style="font-size:80%">Oktober 2021 - April 2022</label>
          </div>
          <div class="text-center div-text-materi" style="flex-basis: 30%">
            <h4 style="font-weight:bold">Program Persiapan</h4>
            <br>
            <label style="font-size:80%">Mabit 2 Bulan 1x di pondok alumni Gontor</label><br>
            <label style="font-size:80%">Info & sharing bersama alumni Gontor</label><br>
            <label style="font-size:80%">Pemantapan mental calon santri</label>
          </div>
          <div class="text-center div-text-materi" style="flex-basis: 30%">
            <h4 style="font-weight:bold">Buku Paket</h4>
            <br>
            <label style="font-size:80%">Buku Tajwid</label><br>
            <label style="font-size:80%">Buku Kumpulan Doa</label><br>
            <label style="font-size:80%">Buku Berhitung</label><br>
            <label style="font-size:80%">Buku Tuntunan Sholat</label><br>
            <label style="font-size:80%">Iqro</label><br>
            <label style="font-size:80%">Alat Tulis dan Buku Tulis</label>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end course section -->

  <section class="course_section layout_padding-bottom" style="background-color:#05819e;padding-bottom:15px !important" id="kursus">
      <div class="course_container" style="padding-top:50px">
        <div class="course_content">
          <div class="text-center div-blue-box" style="flex-basis: 30%">
            <h4 class="detail-blue-box" style="color:#fff">BIAYA PENDAFTARAN</h4>
            <h4 class="detail-blue-box" style="color:#fff;font-weight:bold">450.000</h4>
            <br>
            <h4 class="detail-blue-box" style="color:#fff">BIAYA BULANAN</h4>
            <h4 class="detail-blue-box" style="color:#fff;font-weight:bold">400.000</h4>
          </div>
          <div class="text-center div-blue-box" style="flex-basis: 30%">
            <h4 class="detail-blue-box" style="color:#fff">Sudah Termasuk</h4>
            <p class="detail-blue-box2" style="font-size:80%;color:#fff">Mabit 2 Bulan 1x di pondok alumni Gontor</p>
            <p class="detail-blue-box2" style="font-size:80%;color:#fff">Info & sharing bersama alumni Gontor</p>
            <p class="detail-blue-box2" style="font-size:80%;color:#fff">Pemantapan mental calon santri</p>
          </div>
          <div class="text-center div-blue-box" style="flex-basis: 30%">
            <a style="font-size:20px;font-weight:bold;width: 200px;margin-top:38px;color:#fff;background-color:#DAA520;border-color:#DAA520;border-radius:25px" class="btn btn-primary" href="{{ route('auth.register' ) }}">Daftar Sekarang</a>
          </div>
        </div>
      </div>
  </section>

  <section class="course_section layout_padding-bottom" style="background-color:#edf4f7" id="kursus">
    <div class="side_img">
      <img src="{{ asset('') }}public/bootstrap/joson/images/side-img.png" alt="" />
    </div>
    <div class="container">
      <div class="heading_container">
        <br>
        <h3>
          Tempat Bimbingan
        </h3>
      </div>
      <div class="course_container">
        <div class="course_content">
          @foreach ($info_helper->location() as $loc)
          {{-- <div class="div-text-materi" style="flex-basis: 25%;margin-right:50px"> --}}
          <div class="col-lg-3">
            <h6 style="font-weight:bold">{{ $loc[0]->regional }}</h6>
            <hr>
            <br>
            @foreach ($loc as $l)
            <div class="div-text-materi">
              <b><label style="font-size:80%">{{ strtoupper($l->name) }}</label></b><br>
              <p style="font-size:70%">{{ $l->address }}</p>
                <i><label style="font-size:70%">({{ $l->description }})</label></i>
              <br>
            </div>
            <br>
            @endforeach
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  <section class="course_section layout_padding-bottom" style="background-color:#06bab5;padding-bottom:15px !important" id="kursus">
    <div class="course_container" style="padding-top:50px">
      <div class="course_content" style="margin:0px 150px">
        <div class="div-blue-box" style="flex-basis: 30%;margin:0px 15px">
          <h4 class="detail-blue-box" style="color:#fff;font-weight:bold">Informasi lebih lanjut :</h4>
          <br>
          <h4 class="detail-blue-box" style="color:#fff;font-size:90%">Ustadz Ginadi Nurbardaneu</h4>
          <h4 class="detail-blue-box" style="color:#fff;font-size:130%">wa.me/6281290982859</h4>
        </div>
        <div class="div-blue-box" style="flex-basis: 30%;margin:0px 15px">
            <h4 class="detail-blue-box" style="color:#06bab5;font-weight:bold">.</h4>
            <br>
            <h4 class="detail-blue-box" style="color:#fff;font-size:90%">Call Center IKPM Bandung</h4>
            <h4 class="detail-blue-box" style="color:#fff;font-size:130%;">wa.me/081312544263</h4>
        </div>
        <div class="div-blue-box" style="flex-basis: 30%;margin:0px 15px">
          <a style="font-size:20px;font-weight:bold;width: 200px;margin-top:38px;color:#fff;background-color:#DAA520;border-color:#DAA520;border-radius:25px" class="btn btn-primary" href="{{ route('auth.register' ) }}">Daftar Sekarang</a>
        </div>
      </div>
    </div>
  </section>

  <!-- login section -->

  <section class="login_section layout_padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="detail-box">
            <div class="row">
              <div class="col-lg-2 text-center">
                <img class="img-responsive" id="img_bimago_academy" src="{{ asset('') }}public/assets/img/logo.png" style="width:200%" alt="">
              </div>
              <div class="col-lg-10" style="padding-left:70px">
                <h3 style="color:#000;text-transform:none !important">
                  Belajar Online Bersama
                </h3>
                <h3 style="color:#000">
                  BIMAGO ACADEMY
                </h3>
              </div>
            </div>
            <br>
            <p style="color:#000">
              Sebagai sarana pendukung pembelajaran BIMAGO,
              BIMAGO ACADEMY memfasilitasi calon santri akses
              belajar 24 jam, dimanapun, kapanpun untuk mengulang
              dan mematangkan materi yang telah disampaikan saat
              bimbingan di BIMAGO Bandung Raya.
            </p>
            @if (Auth::check())
              <a style="color:black;background-color:transparent;border-color:#DAA520;border-radius:25px" href="{{ route('academy.sso') }}">
                Info Lebih Lanjut
              </a>
              @else
              <a style="color:black;background-color:transparent;border-color:#DAA520;border-radius:25px" href="{{ route('academy.redirect') }}">
                Info Lebih Lanjut
              </a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="course_section layout_padding-bottom" style="background-color:#e0e0e0" id="kursus">
    <div class="side_img">
      <img src="{{ asset('') }}public/bootstrap/joson/images/side-img.png" alt="" />
    </div>
    <div class="container">
      <div class="heading_container">
        <br>
        <h3>
          Features
        </h3>
      </div>
      <div class="course_container">
        <div class="course_content">
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/f1.png" style="width:85px;height:85px" alt="" />
            <p style="font-weight:bold">Video Interaktif</p>
            <p style="font-size:80%">Dapatkan akses untuk belajar melalui kumpulan video materi persiapan ujian masuk Gontor lengkap.</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/f2.png" style="width:85px;height:85px" alt="" />
            <p style="font-weight:bold">Belajar & Sharing Via Zoom</p>
            <p style="font-size:80%">1x dalam sebulan belajar & sharing seputar Gontor bersama alumni Gontor.</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/f3.png" style="width:85px;height:85px" alt="" />
            <p style="font-weight:bold">1x Pembayaran</p>
            <p style="font-size:80%">Cukup dengan 1x pembayaran video bisa diputar berkali2 hingga mahir.</p>
          </div>
          <div style="flex-basis:20%" class="div-materi text-center">
            <img src="{{ asset('') }}public/assets/img/f4.png" style="width:85px;height:85px" alt="" />
            <p style="font-weight:bold">Pahala Infaq</p>
            <p style="font-size:80%">Sebagian dari pembayaran disalurkan untuk anak yatim Alittihad yang sedang menempuh pendidikan di pesantren.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="course_section layout_padding-bottom" style="background-color:#fff" id="">
    <div class="side_img">
      <img src="{{ asset('') }}public/bootstrap/joson/images/side-img.png" alt="" />
    </div>
    <div class="container">
      <div class="heading_container">
        <br>
        <h3>
          Gallery
        </h3>
      </div>
      <div class="row" id="">
				<div class="text-center" style="height:auto">
					<div class="row div-gallery1">
						<div class="col-lg-12" id="div_gallery_1" style="padding:0px 150px">
							<img class="card-img-top" src="{{ asset('') }}public/assets/img/gallery/1.jpeg" alt="">
						</div>
					</div>
					<div class="row div-gallery2" id="div_gallery_2" style="padding:0px 135px">
						<div class="col-lg-4">
							<img class="card-img-top" src="{{ asset('') }}public/assets/img/gallery/bimago_santri1.png" alt="">
						</div>
						<div class="col-lg-4">
							<img class="card-img-top" src="{{ asset('') }}public/assets/img/gallery/bimago_santri2.png" alt="">
						</div>
						<div class="col-lg-4">
							<img class="card-img-top" src="{{ asset('') }}public/assets/img/gallery/bimago_santri3.png" alt="">
						</div>
					</div>
				</div>
			</div>
    </div>
  </section>

  <script>
  </script>
  <!-- end contact section -->
  @endsection