<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Admin Bimago</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
    <link rel="icon" href="{{ asset('') }}public/assets/img/logo.ico">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/login_v12/css/main.css">
    @jquery
    @toastr_css
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{ asset('') }}public/bootstrap/login_v12/images/img-01.jpg');">
			<div class="wrap-login100 p-t-190 p-b-30">
				<form class="login100-form validate-form" method="POST" action="{{ route('auth.own.check') }}" >
					{{ csrf_field() }}
					<div class="login100-form-avatar text-center" style="background-color:white">
						<img src="{{ asset('') }}public/assets/img/bimago-bdg.png" style="width:90%" alt="">
					</div>

					<span class="login100-form-title p-t-20 p-b-45">
						Login Admin Bimago
					</span>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "email is required">
						<input class="input100" type="text" name="email" placeholder="email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
                    <br>
                    <div class="text-center w-full">
                        <i class="fa fa-long-arrow-left"></i>						
						<a class="txt1" href="{{ route('home') }}" style="color:black">
							Kembali ke web
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{ asset('') }}public/bootstrap/login_v12/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/bootstrap/login_v12/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ asset('') }}public/bootstrap/login_v12/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/bootstrap/login_v12/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('') }}public/bootstrap/login_v12/js/main.js"></script>
    @toastr_js
    @toastr_render
</body>
</html>