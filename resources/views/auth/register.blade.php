@extends('_template', ['menu' => 'register'])
@section('content')
<!-- ======= Contact Us Section ======= -->
<style>

</style>
<section id="contact" class="" style="">
    <br>
    <div class="container">
        <div class="section-title text-center ml-2 mr-2">
            <a href="{{ route('home') }}">
                <img class="" src="{{ asset('public/assets/img/bimago-bdg.png') }}" style="width:100px" alt="">
            </a>
            <p></p>
            <h1 id="title" style="font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;font-size:25px">REGISTRASI SISWA</h1>
        </div>
        <hr>
        <form action="{{ route("auth.register.act") }}" method="POST" role="form" id="form">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-7">
                    {{ csrf_field() }}
                   <i><label for="">Identitas Diri :</label></i>
                    <div class="form-group mt-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="address" id="address" placeholder="Alamat" required>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="provinsi" style="width:100%" required id="provinsi" class="js-example-basic-single">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="regional" disabled style="width:100%" required id="regional" class="js-example-basic-single">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="kecamatan" disabled style="width:100%" required id="kecamatan" class="js-example-basic-single">
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="name_provinsi" id="name_provinsi">
                            <input type="hidden" name="name_regional" id="name_regional">
                            <input type="hidden" name="name_kecamatan" id="name_kecamatan">
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="birthplace" id="birthplace" placeholder="Tempat Lahir" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="birthdate" id="birthdate" placeholder="Tanggal Lahir" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Nama Orang Tua" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="parent_phone" id="parent_phone" placeholder="No Hp Orang Tua" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="school" id="school" placeholder="Asal Sekolah" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="graduated" required id="graduated" class="form-control">
                                        <option value="">-Pilih Lulusan-</option>
                                        <option value="SD/MI">SD/MI</option>
                                        <option value="SMP/MTs">SMP/MTs</option>
                                        <option value="SMA/MA">SMA/MA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <i><label for="">Pilihan Bimbel :</label></i>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="id_tutor_type" id="id_tutor_type" style="width:100%" class="js-example-basic-single" required>
                                        <option value="">--Pilih Tipe Bimble--</option>
                                        @foreach ($tutor_type as $tt)
                                            <option value="{{ $tt->id }}">{{ $tt->type }}</option>                                        
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select style="width:100%" name="id_learn_place" id="id_learn_place" class="js-example-basic-single">
                                        <option value="">--Pilih Tempat Belajar Regular--</option>
                                        @foreach ($learn_place as $lp)
                                            <option value="{{ $lp->id }}">{{ $lp->name }} - {{ $lp->address }}</option>                                        
                                        @endforeach
                                    </select><br>
                                    <br>
                                    <button style="display:none;width:100%" type="button" class="btn btn-info btn-md" id="badge_recommend"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3" id="div_private" style="display:none">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="request_place" id="request_place" class="form-control">
                                        <option value="">-Pilih Jadwal Belajar (Hari)-</option>
                                        <option value="senin">Senin</option>
                                        <option value="selasa">Selasa</option>
                                        <option value="rabu">Rabu</option>
                                        <option value="kamis">Kamis</option>
                                        <option value="jumat">Jumat</option>
                                        <option value="sabtu">Sabtu</option>
                                        <option value="minggu">Minggu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="request_time" id="request_time" class="form-control">
                                        <option value="">-Pilih Jadwal Belajar (Waktu)-</option>
                                        @for ($i = 0; $i < 25; $i++)
                                            <option value="{{ sprintf("%02d", $i) }}:00">{{ sprintf("%02d", $i) }}:00</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <i><label for="">Kredensial Login :</label></i>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                    <i><label style="font-size:12px" for="" id="passwordStrenght"></label></i>
                                    <i><label style="font-size:12px" for="" id="passWarn"></label></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center"><button style="width:100%" class="btn btn-success" id="btn_submit" type="submit">Daftar</button></div>
            <br>
            <a href="{{ route('auth.login') }}" style="color:dodgerblue" href="">Sudah punya punya akun santri? Masuk disini</a>
            &nbsp | &nbsp
            <a style="color:orangered" href="{{ route('home') }}">Kembali ke halaman depan</a>
        </form>

    </div>
    <div class="modal fade" id="modal_rekomendasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bimago Terdekat Dari Anda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table-responsive table">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>Regional</th>
                            <th>Alamat</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tbody_rekomendasi">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</section><!-- End Contact Us Section -->
<br>
<script type="text/javascript" src="{{ asset('public/../resources/views/auth/main.js') }}"></script>
<script>
    $(document).on("click", "#badge_recommend", function(){
        $("#modal_rekomendasi").modal("show")
    })

    $(document).on('keyup', '#password', function() {
        var pass = $(this).val();

        var strength = 1;
        var arr = [/.{5,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/, /[!@#$%^&*]+/];
        jQuery.map(arr, function(regexp) {
            if(pass.match(regexp))
            strength++;
        });

        if (strength == 1){
            $('#passwordStrenght').html('')
        } else if (strength == 2){
            $('#passwordStrenght').html('Kekuatan Password : Sangat Lemah')
            $('#passwordStrenght').css('color','Red')
        } else if (strength == 3){
            $('#passwordStrenght').html('Kekuatan Password : Lemah')
            $('#passwordStrenght').css('color','Crimson')
        } else if (strength == 4){
            $('#passwordStrenght').html('Kekuatan Password : Cukup')
            $('#passwordStrenght').css('color','DarkOrange')
        } else if (strength == 5){
            $('#passwordStrenght').html('Kekuatan Password : Kuat')
            $('#passwordStrenght').css('color','LimeGreen')
        } else if (strength == 6){
            $('#passwordStrenght').html('Kekuatan Password : Sangat Kuat')
            $('#passwordStrenght').css('color','Green')
        } else if (strength == 0){
            $('#passwordStrenght').html('')
        }

        if (pass.length < 8)
        {
            $("#btn_submit").attr("disabled", true)
            $('#passWarn').html('! Password harus terdiri dari minimal 8 karakter !')
            $('#passWarn').css('color','Red')
        } else
        {
            $("#btn_submit").attr("disabled", false)
            $('#passWarn').html('')
        }
        
    });

    $(document).on("change", "#regional", function(){
        $("#kecamatan").attr("disabled", true)
        $("#kecamatan").empty()

        var val     = $(this).val()
        var route   = "{{ route('data.kecamatan.show',":regional") }}"
        route       = route.replace(":regional", val)

        $.get(route, function(data, status, xhr){
            if (xhr.status == 200)
            {
                $('#kecamatan').append("<option value=''>--Pilih Kecamatan--</option>");

                $.each(data.data, function(idx, el){
                    $('#kecamatan').append("<option value='"+el.name+"'>"+el.name+"</option>");
                })

                $("#kecamatan").attr("disabled", false)
            }
        })
    })

    $(document).on("change", "#kecamatan", function(){
        var provinsi= $("#provinsi option:selected").text()
        provinsi    = provinsi.replace(" ","_")
        var regional= $("#regional option:selected").text()
        regional    = regional.replace(" ","_")
        var kecamatan= $("#kecamatan option:selected").text()
        kecamatan    = kecamatan.replace(" ","_")
        var route   = "{{ route('data.lokasi.show',[":provinsi", ":regional", ":kecamatan"]) }}"
        route       = route.replace(":provinsi", provinsi)
        route       = route.replace(":regional", regional)
        route       = route.replace(":kecamatan", kecamatan)
        $.get(route, function(data, status, xhr){
            if (data.stat != 0)
            {
                $("#tbody_rekomendasi").empty()

                if (data.stat == 2)
                {
                    $.each(data.data, function(idx, el){
                        $("#tbody_rekomendasi").append("<tr>")
                        $("#tbody_rekomendasi").append("<td>"+el.name+"</td>")
                        $("#tbody_rekomendasi").append("<td>"+el.regional+"</td>")
                        $("#tbody_rekomendasi").append("<td>"+el.address+"</td>")
                        $("#tbody_rekomendasi").append("<td><button class='btn btn-success btn-pilih' data-id='"+el.id+"'>Pilih</button></td>")
                        $("#tbody_rekomendasi").append("</tr>")

                        $("#badge_recommend").text("Terdapat rekomendasi Bimago di Kota/Kabupaten anda!")
                        $("#badge_recommend").show()
                    })
                }
                else if (data.stat == 3)
                {
                    $.each(data.data, function(idx, el){
                        $("#tbody_rekomendasi").append("<tr>")
                        $("#tbody_rekomendasi").append("<td>"+el.name+"</td>")
                        $("#tbody_rekomendasi").append("<td>"+el.regional+"</td>")
                        $("#tbody_rekomendasi").append("<td>"+el.address+"</td>")
                        $("#tbody_rekomendasi").append("<td><button class='btn btn-success btn-pilih' data-id='"+el.id+"'>Pilih</button></td>")
                        $("#tbody_rekomendasi").append("</tr>")

                        $("#badge_recommend").text("Terdapat rekomendasi Bimago di Provinsi anda!")
                        $("#badge_recommend").show()
                    })
                } 
                else if (data.stat == 1)
                {
                    $("#tbody_rekomendasi").append("<tr>")
                    $("#tbody_rekomendasi").append("<td>"+data.data.name+"</td>")
                    if (data.data.regional == 1)
                    {
                        $("#tbody_rekomendasi").append("<td>Kota Bandung</td>")
                    } else if (data.data.regional == 2)
                    {
                        $("#tbody_rekomendasi").append("<td>Kabupaten Bandung</td>")
                    } else if (data.data.regional == 3)
                    {
                        $("#tbody_rekomendasi").append("<td>Kabupaten Bandung Barat</td>")
                    } else if (data.data.regional == 4)
                    {
                        $("#tbody_rekomendasi").append("<td>Kota Cimahi</td>")
                    }
                    $("#tbody_rekomendasi").append("<td>"+data.data.address+"</td>")
                    $("#tbody_rekomendasi").append("<td><button class='btn btn-success btn-pilih' data-id='"+data.data.id+"'>Pilih</button></td>")
                    $("#tbody_rekomendasi").append("</tr>")   

                    $("#badge_recommend").text("Terdapat rekomendasi Bimago di DEKAT anda!")
                    $("#badge_recommend").show()
                }
            }
        })
    })

    $(document).on("change", "#id_tutor_type", function(){
        var val = $(this).val()
        checkType(val)
    })

    $(document).on("click", ".btn-pilih", function(){
        var val = $(this).data("id")
        $("#id_learn_place").val(val).trigger("change")
        $("#modal_rekomendasi").modal("hide")
    })

    $(document).ready(function(){
        $('.js-example-basic-single').select2();
        $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/provinsi", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#provinsi").append("<option value=''>--Pilih Provinsi--</option>")
                $.each(data2.data, function(idx,el){
                  $("#provinsi").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#e_provinsi").append("<option value=''>--Pilih Provinsi--</option>")
                $.each(data2.data, function(idx,el){
                  $("#e_provinsi").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
            }
          })
        }
      })
        // var route = "{{ route('data.kecamatan') }}"
        // $.get(route, function(data, status, xhr){
        //     if (xhr.status == 200)
        //     {
        //         $("#spinner").remove()
        //         if (data == 'null')
        //         {
        //             $("#div_kecamatan").append('<input name="kecamatan" id="kecamatan" class="form-control" required placeholder="Kecamatan anda">')
        //         } else
        //         {
        //             $("#div_kecamatan").append('<select name="kecamatan" id="kecamatan" style="width:100%" class="js-example-basic-single" required><option value="">--Pilih Kecamatan--</option></select>')
        //             $.each(data, function(idx, data){
        //                 $.each(data, function(idx2, data2){
        //                     $('#kecamatan').append("<option value='"+data2.name+"'>"+data2.name+"</option>");
        //                 })
        //             })
        //             $('.js-example-basic-single').select2();
        //         }
        //     }
        // })
    })

    $(document).on('change', '#provinsi', function(){
      $("#regional").attr("disabled", true)
      var id_provinsi = $(this).val()

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/kabupaten?idpropinsi="+id_provinsi+"", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#regional").empty()
                $("#regional").append("<option value=''>--Pilih Kota/Kabupaten--</option>")
                $.each(data2.data, function(idx,el){
                  $("#regional").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#regional").attr("disabled", false)
            }
          })
        }
      })
    })

    $(document).on('change', '#regional', function(){
      $("#kecamatan").attr("disabled", true)
      var id_regional = $(this).val()

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/kecamatan?idkabupaten="+id_regional+"", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#kecamatan").empty()
                $("#kecamatan").append("<option value=''>--Pilih Kecamatan--</option>")
                $.each(data2.data, function(idx,el){
                  $("#kecamatan").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#kecamatan").attr("disabled", false)
            }
          })
        }
      })
    })

    $(document).on('change', '#provinsi', function(){
      $("#name_provinsi").val($("#provinsi option:selected").text())
    })

    $(document).on('change', '#regional', function(){
      $("#name_regional").val($("#regional option:selected").text())
    })
    
    $(document).on('change', '#kecamatan', function(){
      $("#name_kecamatan").val($("#kecamatan option:selected").text())
    })

</script>

@endsection