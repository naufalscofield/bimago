@extends('template', ['menu' => 'login'])

@section('content')
    
<style>
  #btn_login:hover {
    background-color: #DAA520 !important;
  }
</style>
<section class="login_section layout_padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="login_form" style="background-color: rgba(218, 165, 32, 0.5) !important">
            <h5 style="color:black">
              Login
            </h5>
            <form action="{{ route('auth.login.check') }}" method="POST">
            {{ csrf_field() }}
              <div>
                <input required type="email" name="email" placeholder="Email Anda" />
              </div>
              <div>
                <input required type="password" name="password" placeholder="Password Anda" />
              </div>
              <button style="margin-top:38px;color:#000;background-color:#fff;border-color:#DAA520;border-radius:25px;width:100%" class="btn btn-primary" id="btn_login" type="submit">Masuk</button><br>
              <br><a style="" href="{{ route('auth.register' ) }}">Daftar Akun Baru</a>
              {{-- <button style="font-size:12px;width: 80px;margin-top:38px;color:#fff;background-color:#DAA520;border-color:#DAA520;border-radius:25px;margin-right:10px" type="submit">Masuk</button> --}}
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end login section -->
  @endsection