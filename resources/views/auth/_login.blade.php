@extends('template')
@section('content')
<!-- ======= Contact Us Section ======= -->
<section id="contact" class="contact section-bg">
    <div class="container">

      <div class="section-title">
        <h2>Masuk Santri</h2>
      </div>

      <div class="row justify-content-center">

        <div class="col-lg-3 col-md-5 mb-5 mb-md-0">
          <div class="info">
            <div class="address text-center">
              <img src="{{ asset('public/assets/img/logo.png') }}" style="width:60%" alt="">
            </div>

            {{-- <div class="email">
              <i class="bx bx-envelope"></i>
              <p>info@example.com</p>
            </div>

            <div class="phone">
              <i class="bx bx-phone-call"></i>
              <p>+1 5589 55488 55s</p>
            </div> --}}
          </div>

          {{-- <div class="social-links">
            <a href="{{ asset('') }}bootstrap/siimple/#" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="{{ asset('') }}bootstrap/siimple/#" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="{{ asset('') }}bootstrap/siimple/#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="{{ asset('') }}bootstrap/siimple/#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
          </div> --}}

        </div>

        <div class="col-lg-5 col-md-7">
          <form action="{{ route("auth.login.check") }}" method="POST" role="form" class="">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="form-control" name="email" id="email" value="{{Request::old('email')}}" placeholder="Email Anda" required>
            </div>
            <div class="form-group mt-3">
                <label for="">Kata Sandi</label>
                <input type="password" class="form-control" name="password" id="password" value="{{Request::old('password')}}"  placeholder="Password Anda" required>
            </div>
            <br>
            <div class="text-center"><button style="width:100%" class="btn btn-primary" type="submit">Masuk</button></div>
            <br>
            <a href="{{ route('auth.register') }}" style="color:rgb(65, 156, 236)" href="">Daftar sebagai santri</a>
            &nbsp | &nbsp
            <a href="{{ route('home') }}">Kembali ke halaman depan</a>
          </form>
        </div>

      </div>

    </div>
  </section><!-- End Contact Us Section -->

  <script>
      $(document).ready(function(){
          
      })
  </script>
@endsection