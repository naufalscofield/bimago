<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env("APP_NAME") }}</title>
</head>
<body>
    <div>
        <br>
        <img src="{{ asset('public/assets/img/grayscale-logo.png') }}" alt="" style="width:150px; margin-left:10px">
        <center>
            <img src="{{ asset('public/assets/img/404.png') }}" alt="" style="width:500px">
             <br>
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size: 20px;">Halaman tidak ditemukan</h1>
            <h1 style="font-family:Arial, Helvetica, sans-serif; font-size: 15px;">Kembali ke halaman depan dalam <label id="count">5</label> detik</h1>
        </center>

    </div>
</body>
</html>

<script type="text/javascript">

var milisec = 0
var seconds = 6
document.getElementById("count").innerHTML = '5';

function display() {
    if (milisec <= 0) {
        milisec = 9
        seconds -= 1
    }
    if (seconds <= -1) {
        milisec = 0
        seconds += 1
    }
    else
        milisec -= 1

    document.getElementById("count").innerHTML = seconds;

    if (seconds == 0)
    {
        var base_url    = window.location.origin;
        window.location.href = "{{ route('home') }}";

    } else
    {
        setTimeout("display()", 100)
    }
}
display()

</script>
