@extends('admin.template', ['title' => 'Admin', 'menu' => 'admin'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Daftar Admin</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
              <button id="btn_add" data-toggle="modal" data-target="#modal_add" class="btn btn-success btn-sm">+ Tambah</button>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Nama
                    </th>
                    <th>
                      Email
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.admin.store') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Nama</label><br>
                    <input type="text" required name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Email</label><br>
                    <input type="email" required name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Password</label><br>
                    <input type="password" required name="password" class="form-control">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      var route = "{{ route('admin.admin.get') }}"
      var table = $('#datatable').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'name', name: 'name'},
              {data: 'email', name: 'email'},
          ],
      });
    })

  </script>
@endsection