@extends('admin.template', ['title' => 'Tipe Bimbel', 'menu' => 'tipe_bimbel'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Tipe Bimbel</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
              <button id="btn_add" data-toggle="modal" data-target="#modal_tambah" class="btn btn-success btn-sm">+ Tambah</button>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Tipe
                    </th>
                    <th>
                      Harga
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Tipe Bimbel</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.tipe.bimbel.update') }}" method="POST">
              {{ csrf_field() }}
              <input name="e_id" id="e_id" type="hidden">
              <div class="form-group">
                  <label for="">Tipe</label><br>
                  <input type="text" required name="e_type" id="e_type" class="form-control">
              </div>
              <div class="form-group">
                  <label for="">Deskripsi</label><br>
                  <input type="text" required name="e_description" id="e_description" class="form-control">
              </div>
              <div class="form-group">
                  <label for="">Harga</label><br>
                  <input type="number" required name="e_price" id="e_price" class="form-control">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Tipe Bimbel</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.tipe.bimbel.store') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Tipe</label><br>
                    <input type="text" required name="type" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Harga</label><br>
                    <input type="number" required name="price" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Deskripsi</label><br>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control" required></textarea>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      var route = "{{ route('admin.tipe.bimbel.get') }}"
      var table = $('#datatable').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'type', name: 'type'},
              {data: 'harga', name: 'harga'},
              {data: 'action', name: 'action'},
          ],
      });
    })

    $(document).on('click', '.btn-delete', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.tipe.bimbel.delete', ":id") }}"
      route = route.replace(":id", id)

      Swal.fire({
        title: 'Apa anda yakin akan menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.value) {
          $.get(route, function(data, status, xhr){
            if (xhr.status == 200)
            {
                location.reload()
            } else
            {
                toastr.error("Gagal menghapus data")
            }
          })
        }
      })
    })

    $(document).on('click', '.btn-edit', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.tipe.bimbel.show', ":id") }}"
      route = route.replace(":id", id)

      $.get(route, function(data, status, xhr){
        if (xhr.status == 200)
        {
          $("#modal_edit").modal("show")
          $("#e_id").val(data.enc_id)
          $("#e_type").val(data.type)
          $("#e_description").val(data.description)
          $("#e_price").val(data.price)
        } else
        {
            toastr.error("Gagal mengambil detail data")
        }
      })
    })
  </script>
@endsection