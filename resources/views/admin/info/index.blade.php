@extends('admin.template', ['title' => 'Info', 'menu' => 'info'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Konfigurasi Info Web</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      Deskripsi Tentang
                    </th>
                    <th>
                      Alamat
                    </th>
                    <th>
                      Email
                    </th>
                    <th>
                      No Telp
                    </th>
                    <th>
                      Link Gmaps
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                    <td>
                      {{ $info->about }}
                    </td>
                    <td>
                      {{ $info->address }}
                    </td>
                    <td>
                      {{ $info->email }}
                    </td>
                    <td>
                      {{ $info->phone }}
                    </td>
                    <td>
                      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15843.684764893325!2d107.6528127!3d-6.9000286!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf6156f6a3663477c!2sPT.%20Solmit%20Bangun%20Indonesia!5e0!3m2!1sen!2sid!4v1634106635832!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </td>
                    <td>
                      <button class='btn btn-success btn-sm' id='btn_edit' data-toggle='modal' data-target='#modal_edit'><i class='fa fa-pencil'></i> Edit</button>
                    </td>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Konfigurasi Info</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.info.update') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Deskripsi Tentang Bimago</label><br>
                    <textarea name="about" required class="form-control" id="" cols="30" rows="10">{{ $info->about }}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Alamat Bimago</label><br>
                    <textarea name="address" required class="form-control" id="" cols="30" rows="10">{{ $info->address }}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Email Bimago</label><br>
                    <input type="email" required name="email" value="{{ $info->email }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">No Telp Bimago</label><br>
                    <input type="number" required name="phone" value="{{ $info->phone }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Link Embed Gmaps Bimago</label><br>
                    <input type="text" name="link_gmaps" value="{{ $info->link_gmaps }}" class="form-control">
                    <span><a target="blank" href="https://www.youtube.com/watch?v=R7m0e-7JCQk">Klik untuk tahu cara mendapatkan link embed gmaps</a></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
  </script>
@endsection