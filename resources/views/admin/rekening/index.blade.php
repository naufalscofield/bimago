@extends('admin.template', ['title' => 'Rekening', 'menu' => 'rekening'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Info Rekening</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      Bank
                    </th>
                    <th>
                      Atas Nama
                    </th>
                    <th>
                      Nomer Rekening
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                    <td>
                      {{ $rekening->bank }}
                    </td>
                    <td>
                      {{ $rekening->name }}
                    </td>
                    <td>
                      {{ $rekening->number }}
                    </td>
                    <td>
                      <button class='btn btn-success btn-sm' id='btn_edit' data-toggle='modal' data-target='#modal_edit'><i class='fa fa-pencil'></i> Edit</button>
                    </td>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Rekening</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.rekening.update') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Bank</label><br>
                    <input type="text" required name="bank" value="{{ $rekening->bank }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Atas Nama</label><br>
                    <input type="text" required name="name" value="{{ $rekening->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">No Rekening</label><br>
                    <input type="number" required name="number" value="{{ $rekening->number }}" class="form-control">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
  </script>
@endsection