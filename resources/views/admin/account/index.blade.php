@extends('admin.template', ['title' => 'Profil', 'menu' => 'profil'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title"><i class="fa fa-id-card"></i> Edit Profil</h4>
            </div>
            <br>
            <div class="card-body">
              <form method="POST" action="{{ route('admin.admin.update') }}">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Nama</label>
                      <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Email</label>
                      <input type="email" value="{{ $user->email }}" name="email" class="form-control">
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Perbarui Profil</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title"><i class="fa fa-key"></i> Ganti Password</h4>
            </div>
            <br>
            <div class="card-body">
              <form method="POST" action="{{ route('admin.account.change.password') }}">
                @csrf
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Password Lama</label>
                      <input type="password" name="old_password" value="" class="form-control">
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Password Baru</label>
                      <input type="password" value="" name="new_password" id="new_password" class="form-control"> 
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Konfirmasi Password Baru</label>
                      <input type="password" value="" id="k_new_password" class="form-control">
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Perbarui Password</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    $("#k_new_password").focusout(function(){
      var k_val = $(this).val()
      var o_val = $("#new_password").val()

      if (k_val !== o_val)
      {
        alert("Konfirmasi Password Baru Tidak Sesuai!")
        $("#k_new_password").val("")
      }
    })
  </script>
@endsection
