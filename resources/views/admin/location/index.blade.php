@extends('admin.template', ['title' => 'Lokasi', 'menu' => 'lokasi'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Lokasi</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
              <button id="btn_add" data-toggle="modal" data-target="#modal_tambah" class="btn btn-success btn-sm">+ Tambah</button>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Nama Lokasi
                    </th>
                    <th>
                      Regional
                    </th>
                    <th>
                      Alamat Lengkap
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Lokasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.lokasi.update') }}" method="POST">
              {{ csrf_field() }}
              <input name="e_id" id="e_id" type="hidden">
              <div class="form-group">
                  <label for="">Nama Lokasi</label><br>
                  <input type="text" required name="e_name" id="e_name" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Provinsi</label><br>
                <select required name="e_provinsi" class="js-example-basic-single" style="width:100%" id="e_provinsi"></select>
              </div>
              <div class="form-group">
                <label for="">Regional (Kota / Kabupaten)</label><br>
                <select disabled required name="e_regional" class="js-example-basic-single" style="width:100%" id="e_regional"></select>
              </div>
              <div class="form-group">
                <label for="">Kecamatan</label><br>
                <select disabled required name="e_kecamatan" class="js-example-basic-single" style="width:100%" id="e_kecamatan"></select>
                {{-- <input type="text" placeholder="Kecamatan membantu untuk merekomendasikan Bimago terdekat dari calon siswa, tidak wajib diisi" name="kecamatan" class="form-control"> --}}
              </div>
              <div class="form-group">
                  <label for="">Alamat Lengkap</label><br>
                  <textarea name="e_address" id="e_address" id="" cols="30" rows="10" class="form-control" required></textarea>
              </div>
              <div class="form-group">
                  <label for="">Deskripsi</label><br>
                  <textarea name="e_description" id="e_description" id="" cols="30" rows="10" class="form-control" required></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_tambah" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Lokasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.lokasi.store') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Nama Lokasi</label><br>
                    <input type="text" required name="name" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Provinsi</label><br>
                  <select required name="provinsi" class="js-example-basic-single" style="width:100%" id="provinsi"></select>
                </div>
                <div class="form-group">
                  <label for="">Regional (Kota / Kabupaten)</label><br>
                  <select disabled required name="regional" class="js-example-basic-single" style="width:100%" id="regional"></select>
                </div>
                <div class="form-group">
                  <label for="">Kecamatan</label><br>
                  <select disabled required name="kecamatan" class="js-example-basic-single" style="width:100%" id="kecamatan"></select>
                  {{-- <input type="text" placeholder="Kecamatan membantu untuk merekomendasikan Bimago terdekat dari calon siswa, tidak wajib diisi" name="kecamatan" class="form-control"> --}}
                </div>
                <div class="form-group">
                  <label for="">Alamat Lengkap</label><br>
                  <textarea name="address" id="" cols="30" rows="10" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                  <label for="">Deskripsi</label><br>
                  <textarea name="description" id="" cols="30" rows="10" class="form-control" required placeholder="Waktu belajar : ....  Contact Person : ...."></textarea>
                </div>
                <input type="hidden" name="name_provinsi" id="name_provinsi">
                <input type="hidden" name="name_regional" id="name_regional">
                <input type="hidden" name="name_kecamatan" id="name_kecamatan">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
    var e_regional
    var e_kecamatan

    $(document).ready(function(){
      $('.js-example-basic-single').select2();

      var route = "{{ route('admin.lokasi.get') }}"
      var table = $('#datatable').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'name', name: 'name'},
              {data: 'regional', name: 'regional'},
              {data: 'alamat_lengkap', name: 'alamat_lengkap'},
              {data: 'action', name: 'action'},
          ],
      });

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/provinsi", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#provinsi").append("<option value=''>--Pilih Provinsi--</option>")
                $.each(data2.data, function(idx,el){
                  $("#provinsi").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#e_provinsi").append("<option value=''>--Pilih Provinsi--</option>")
                $.each(data2.data, function(idx,el){
                  $("#e_provinsi").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
            }
          })
        }
      })

    })

    $(document).on('click', '.btn-delete', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.lokasi.delete', ":id") }}"
      route = route.replace(":id", id)

      Swal.fire({
        title: 'Apa anda yakin akan menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.value) {
          $.get(route, function(data, status, xhr){
            if (xhr.status == 200)
            {
                location.reload()
            } else
            {
                toastr.error("Gagal menghapus data")
            }
          })
        }
      })
    })

    $(document).on('click', '.btn-edit', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.lokasi.show', ":id") }}"
      route = route.replace(":id", id)

      $.get(route, function(data, status, xhr){
        if (xhr.status == 200)
        {
          $("#modal_edit").modal("show")
          $("#e_id").val(data.enc_id)
          $("#e_name").val(data.name)
          $("#e_kecamatan").val(data.kecamatan)
          $("#e_address").val(data.address)
          $("#e_description").val(data.description)

          let val_prov = $("#e_provinsi").find("option:contains('"+data.provinsi+"')").val()
          $("#e_provinsi").val(val_prov).trigger('change.select2');

          e_regional  = data.regional
          e_kecamatan = data.kecamatan

          $.get("https://x.rajaapi.com/poe", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
              $.get("https://x.rajaapi.com/MeP7c5ne"+data2.token+"/m/wilayah/kabupaten?idpropinsi="+$("#e_provinsi").val()+"", function(data3, status3, xhr3){
                if (xhr3.status == 200)
                {
                  $("#e_regional").empty()
                  $("#e_regional").attr("disabled", false)
                  $("#e_regional").append("<option value=''>--Pilih Kota/Kabupaten--</option>")
                  $.each(data3.data, function(idx,el){
                    $("#e_regional").append('<option value="'+el.id+'">'+el.name+'</option>')
                  })

                  let val_reg = $("#e_regional").find("option:contains('"+e_regional+"')").val()
                  $("#e_regional").val(val_reg).trigger('change.select2');

                  $.get("https://x.rajaapi.com/MeP7c5ne"+data2.token+"/m/wilayah/kecamatan?idkabupaten="+$("#e_regional").val()+"", function(data4, status4, xhr4){
                    if (xhr4.status == 200)
                    {
                      $("#e_kecamatan").empty()
                      $("#e_kecamatan").attr("disabled", false)
                      $("#e_kecamatan").append("<option value=''>--Pilih Kecamatan--</option>")
                      $.each(data4.data, function(idx,el){
                        $("#e_kecamatan").append('<option value="'+el.id+'">'+el.name+'</option>')
                      })

                      let val_kec = $("#e_kecamatan").find("option:contains('"+e_kecamatan.toUpperCase()+"')").val()
                      $("#e_kecamatan").val(val_kec).trigger('change.select2');
                    }
                  })
                }
              })
            }
          })
        } else
        {
            toastr.error("Gagal mengambil detail data")
        }
      })
    })

    $(document).on('change', '#provinsi', function(){
      $("#regional").attr("disabled", true)
      var id_provinsi = $(this).val()

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/kabupaten?idpropinsi="+id_provinsi+"", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#regional").empty()
                $("#regional").append("<option value=''>--Pilih Kota/Kabupaten--</option>")
                $.each(data2.data, function(idx,el){
                  $("#regional").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#regional").attr("disabled", false)
            }
          })
        }
      })
    })

    $(document).on('change', '#regional', function(){
      $("#kecamatan").attr("disabled", true)
      var id_regional = $(this).val()

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/kecamatan?idkabupaten="+id_regional+"", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#kecamatan").empty()
                $("#kecamatan").append("<option value=''>--Pilih Kecamatan--</option>")
                $.each(data2.data, function(idx,el){
                  $("#kecamatan").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
                $("#kecamatan").attr("disabled", false)
            }
          })
        }
      })
    })

    $(document).on('change', '#provinsi', function(){
      $("#name_provinsi").val($("#provinsi option:selected").text())
    })

    $(document).on('change', '#regional', function(){
      $("#name_regional").val($("#regional option:selected").text())
    })
    
    $(document).on('change', '#kecamatan', function(){
      $("#name_kecamatan").val($("#kecamatan option:selected").text())
    })

  </script>
@endsection