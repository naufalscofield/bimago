@extends('admin.template', ['title' => 'SPP', 'menu' => 'spp'])

@section('content')
<style>
  .blink_me {
    animation: blinker 1s linear infinite;
  }

  @keyframes blinker {
    50% {
      opacity: 0;
    }
  }

  /* Absolute Center Spinner */
  .loading {
    position: fixed;
    z-index: 999;
    height: 2em;
    width: 2em;
    overflow: show;
    margin: auto;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }

  /* Transparent Overlay */
  .loading:before {
    content: '';
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
      background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

    background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

  /* :not(:required) hides these rules from IE9 and below */
  .loading:not(:required) {
    /* hide "loading..." text */
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }

  .loading:not(:required):after {
    content: '';
    display: block;
    font-size: 10px;
    width: 1em;
    height: 1em;
    margin-top: -0.5em;
    -webkit-animation: spinner 150ms infinite linear;
    -moz-animation: spinner 150ms infinite linear;
    -ms-animation: spinner 150ms infinite linear;
    -o-animation: spinner 150ms infinite linear;
    animation: spinner 150ms infinite linear;
    border-radius: 0.5em;
    -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  }

  /* Animation */

  @-webkit-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-moz-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-o-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
</style>
<div class="loading" style="display:none">Loading&#8230;</div>
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Pembayaran SPP</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Nama Siswa
                    </th>
                    <th>
                      Lokasi Belajar
                    </th>
                    <th>
                      Tipe Bimbel
                    </th>
                    <th>
                      Total Pembayaran
                    </th>
                    <th>
                      Periode
                    </th>
                    <th>
                      Jumlah Bulan
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_verifikasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Verifikasi Pembayaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.spp.verifikasi') }}" method="POST">
              {{ csrf_field() }}
              <input name="id" id="id" type="hidden">
              <table class="table" border="1">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Tipe</th>
                    <th>Total</th>
                    <th>Periode</th>
                    <th>Jumlah Bulan</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td id="t_nama"></td>
                    <td id="t_tipe"></td>
                    <td id="t_total"></td>
                    <td id="t_periode"></td>
                    <td id="t_bulan"></td>
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="text-center">
                <img src="" id="receipt" style="width:50%;height:50%" alt="">
              </div>
              <br>
              <div class="form-group" id="div_keterangan" style="display:none">
                  <label for="">Keterangan</label><br>
                  <textarea name="keterangan" id="keterangan" required class="form-control" cols="30" rows="10">-</textarea>
              </div>
              <div class="form-group">
                  <label for="">Status Verifikasi</label><br>
                  <select required name="status" class="form-control" id="status">
                    <option value="">Pilih Status</option>
                    <option value="3">Pembayaran Diterima</option>
                    <option value="2">Pembayaran Ditolak</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Verifikasi</button>
            </div>
          </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      var route = "{{ route('admin.spp.get') }}"
      var table = $('#datatable').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'nama', name: 'nama'},
              {data: 'location', name: 'location'},
              {data: 'tipe', name: 'tipe'},
              {data: 'total', name: 'total'},
              {data: 'periode', name: 'periode'},
              {data: 'bulan', name: 'bulan'},
              {data: 'action', name: 'action'},
          ],
      });

      var sess = "{{ Session::get('notification') }}"
      if (sess !== '')
      {
        openVerification(sess)
      }

    })

    function openVerification(id)
    {
      var route = "{{ route('admin.spp.show', ":id") }}"
      route = route.replace(":id", id)

      $.get(route, function(data, status, xhr){
        if (xhr.status == 200)
        {
          $("#modal_verifikasi").modal("show")
          $("#id").val(data.id_payment)
          $("#t_nama").text(data['user'].name)
          $("#t_tipe").text(data['user']['biodata']['tipe_bimbel'].type)
          $("#t_total").text(data.total_amount)
          $("#t_periode").text(data.periode)
          $("#t_bulan").text(data.bulan)
          $("#receipt").attr("src", data.receipt_path)
        } else
        {
            toastr.error("Gagal mengambil detail data")
        }
      })
    }

    $(document).on('click', '.btn-verifikasi-manual', function(){
      var id = $(this).data("id")
      Swal.fire({
        title: 'Apakah anda yakin akan mengubah status pembayaran ini menjadi DITERIMA?',
        text: "Tindakan ini tidak dapat dibatalkan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal'
      }).then((result) => {
        $(".loading").show()
        if (result.value) {
          var route = "{{ route('admin.spp.verifikasi.manual', ":id") }}"
          route     = route.replace(":id", id)

          $.get(route, function(data, status, xhr){
            $(".loading").hide()
            if (xhr.status == 200)
            {
              toastr.success('Pembayaran berhasil diverifikasi terima manual')
              setInterval(function(){ location.reload() }, 2000);
            } else if (xhr.status == 400)
            {
              toastr.success('Terjadi kesalahan saat memverifikasi')
            } else if (xhr.status == 403)
            {
              window.location.href = "{{ route('admin.spp.403')}}";
            }
          })
        }
      })
    })

    $(document).on('click', '.btn-verifikasi', function(){
      var id = $(this).data("id")
      openVerification(id)
    })

    $("a").trigger("click");
    $(document).on('change', '#status', function(){
      if ($(this).val() == 2)
      {
        $("#div_keterangan").show()
        $("#keterangan").val("*Contoh : Total pembayaran kurang dari tagihan, mohon lengkapi/hubungi nomer berikut 081xxxxxx")
      } else
      {
        $("#div_keterangan").hide()
        $("#keterangan").val("-")
      }
    })
  </script>
@endsection