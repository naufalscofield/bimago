@extends('admin.template', ['title' => 'Regional', 'menu' => 'regional'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Daftar Regional (Kota/Kab)</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
              <button id="btn_add" data-toggle="modal" data-target="#modal_add" class="btn btn-success btn-sm">+ Tambah</button>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Provinsi
                    </th>
                    <th>
                      Kota/Kab
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_add" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Regional</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.regional.store') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Provinsi</label><br>
                    <select required name="provinsi" class="js-example-basic-single" style="width:100%" id="provinsi"></select>
                </div>
                <div class="form-group">
                  <label for="">Kota / Kabupaten</label><br>
                  <select disabled required name="regional" class="js-example-basic-single" style="width:100%" id="regional"></select>
                </div>
                <input type="hidden" id="name_provinsi" required name="name_provinsi" class="form-control">
                <input type="hidden" id="name_regional" required name="name_regional" class="form-control">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      $('.js-example-basic-single').select2()
      var route = "{{ route('admin.regional.get') }}"
      var table = $('#datatable').DataTable({
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'provinsi', name: 'provinsi'},
              {data: 'regional', name: 'regional'},
              {data: 'action', name: 'action'},
          ],
      });

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/provinsi", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#provinsi").append("<option value=''>--Pilih Provinsi--</option>")
                $.each(data2.data, function(idx,el){
                  $("#provinsi").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
            }
          })
        }
      })
    })

    $(document).on('change', '#provinsi', function(){
      var id_provinsi = $(this).val()

      $.get("https://x.rajaapi.com/poe", function(data, status, xhr){
        if (xhr.status == 200)
        {
          $.get("https://x.rajaapi.com/MeP7c5ne"+data.token+"/m/wilayah/kabupaten?idpropinsi="+id_provinsi+"", function(data2, status2, xhr2){
            if (xhr2.status == 200)
            {
                $("#regional").empty()
                $("#regional").attr("disabled", false)
                $("#regional").append("<option value=''>--Pilih Kota/Kabupaten--</option>")
                $.each(data2.data, function(idx,el){
                  $("#regional").append('<option value="'+el.id+'">'+el.name+'</option>')
                })
            }
          })
        }
      })
    })

    $(document).on('change', '#provinsi', function(){
      $("#name_provinsi").val($("#provinsi option:selected").text())
    })

    $(document).on('change', '#regional', function(){
      $("#name_regional").val($("#regional option:selected").text())
    })

    $(document).on('click', '.btn-delete', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.regional.delete', ":id") }}"
      route = route.replace(":id", id)

      Swal.fire({
        title: 'Apa anda yakin akan menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {
        if (result.value) {
          $.get(route, function(data, status, xhr){
            if (xhr.status == 200)
            {
                location.reload()
            } else
            {
                toastr.error("Gagal menghapus data")
            }
          })
        }
      })
    })

  </script>
@endsection