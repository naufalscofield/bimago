@extends('admin.template', ['title' => 'Siswa', 'menu' => 'siswa'])

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Daftar Siswa</h4>
              {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="datatable">
                  <thead class=" text-primary">
                    <th>
                      No
                    </th>
                    <th>
                      Nama Lengkap
                    </th>
                    <th>
                      Tempat Belajar
                    </th>
                    <th>
                      Email
                    </th>
                    <th>
                      Tanggal Daftar
                    </th>
                    <th>
                      Lulusan
                    </th>
                    <th>
                      No Telp Orang Tua
                    </th>
                    <th>
                      <i class="fa fa-cogs"></i>
                    </th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detail Siswa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>Nama</td>
              <td id="d_nama"></td>
            </tr>

            <tr>
              <td>Tempat Belajar</td>
              <td id="d_location"></td>
            </tr>

            <tr>
              <td>Email</td>
              <td id="d_email"></td>
            <tr>

            <tr>
              <td>Tanggal Daftar</td>
              <td id="d_tanggal_daftar"></td>
            <tr>

            <tr>
              <td>Tempat Tanggal Lahir</td>
              <td id="d_ttl"></td>
            <tr>

            <tr>
              <td>Lulusan</td>
              <td id="d_lulusan"></td>
            <tr>

            <tr>
              <td>Nama Orang Tua</td>
              <td id="d_ortu"></td>
            <tr>

            <tr>
              <td>No Telp Orang Tua</td>
              <td id="d_no_telp_ortu"></td>
            <tr>

            <tr>
              <td>Alamat</td>
              <td id="d_alamat"></td>
            <tr>

          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
  <script>
    $(document).ready(function(){
      var route = "{{ route('admin.siswa.get') }}"
      var table = $('#datatable').DataTable({ 
          responsive: true,
          processing: true,
          serverSide: true,
          ajax: route,
          columns: [
              {data: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'nama', name: 'nama'},
              {data: 'location', name: 'location'},
              {data: 'email', name: 'email'},
              {data: 'tanggal_daftar', name: 'tanggal_daftar'},
              {data: 'lulusan', name: 'lulusan'},
              {data: 'no_telp_org_tua', name: 'no_telp_org_tua'},
              {data: 'action', name: 'action'},
          ],
          dom: 'Bfrtip',
          buttons: [
              'excel'
          ]
      });
    })

    $(document).on('click', '.btn-detail', function(){
      var id = $(this).data("id")
      var route = "{{ route('admin.siswa.show', ":id") }}"
      route = route.replace(":id", id)

      $.get(route, function(data, status, xhr){
        if (xhr.status == 200)
        {
          $("#modal_detail").modal("show")
          $("#d_nama").text(": "+data.name)
          $("#d_location").text(": "+data['biodata']['location'].name)
          $("#d_email").text(": "+data.email)
          $("#d_tanggal_daftar").text(": "+data.tanggal_daftar)
          $("#d_ttl").text(": "+data.ttl)
          $("#d_lulusan").text(": "+data['biodata'].graduated+" - "+data['biodata'].school)
          $("#d_ortu").text(": "+data['biodata'].parent_name)
          $("#d_no_telp_ortu").text(": "+data['biodata'].parent_phone)
          $("#d_alamat").text(": "+data['biodata'].address)
        } else
        {
            toastr.error("Gagal mengambil detail data")
        }
      })
    })
  </script>
@endsection