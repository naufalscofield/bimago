<!DOCTYPE html>
<html lang="en">
@if (Request::segment(1) == 'login')
  <style>
    footer {
      position: fixed;
      height: 100px;
      bottom: 0;
      width: 100%;
    }
  </style>
@endif
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Bimago</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  {{-- <link href="{{ asset('public/bootstrap/siimple/assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('public/bootstrap/siimple/assets/img/apple-touch-icon.png"') }}" rel="apple-touch-icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,600,700&display=swap" rel="stylesheet" />

  <link href="{{ asset('public/bootstrap/siimple/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/bootstrap/siimple/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link rel="icon" href="{{ asset('') }}public/assets/img/logo.ico">

  <link href="{{ asset('public/bootstrap/siimple/assets/css/style.css') }}" rel="stylesheet">
  @jquery
  @toastr_css --}}

  <link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/joson/css/bootstrap.css" />
  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,600,700&display=swap" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{{ asset('') }}public/bootstrap/joson/css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="{{ asset('') }}public/bootstrap/joson/css/responsive.css" rel="stylesheet" />
  <link rel="icon" href="{{ asset('') }}public/assets/img/logo.ico">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  @jquery
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  @toastr_css

  <!-- =======================================================
  * Template Name: Siimple - v4.3.0
  * Template URL: https://bootstrapmade.com/free-bootstrap-landing-page/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body class="section" style="background-color:#FAFAD2">

  @if (!in_array(Request::segment(1), ['login', 'register']))
    <!-- ======= Header ======= -->
    <header id="header">
      <div class="container-fluid">

        <div class="">
          <h1><a href="{{ route('home') }}"><img src="{{ asset('public/assets/img/logo.png') }}" style="height:8%;width:8%" alt=""></a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
        </div>

        <button type="button" class="nav-toggle"><i class="bx bx-menu"></i></button>
        <nav class="nav-menu">
          <ul>
            <li class="active"><a href="{{ route('home') }}" class="scrollto">Beranda</a></li>
            <li><a href="{{ asset('') }}bootstrap/siimple/#about" class="scrollto">Tentang Kami</a></li>
            {{-- <li class="drop-down"><a href="{{ asset('') }}bootstrap/siimple/">Drop Down</a>
              <ul>
                <li><a href="{{ asset('') }}bootstrap/siimple/#">Drop Down 1</a></li>
                <li class="drop-down"><a href="{{ asset('') }}bootstrap/siimple/#">Drop Down 2</a>
                  <ul>
                    <li><a href="{{ asset('') }}bootstrap/siimple/#">Deep Drop Down 1</a></li>
                    <li><a href="{{ asset('') }}bootstrap/siimple/#">Deep Drop Down 2</a></li>
                    <li><a href="{{ asset('') }}bootstrap/siimple/#">Deep Drop Down 3</a></li>
                    <li><a href="{{ asset('') }}bootstrap/siimple/#">Deep Drop Down 4</a></li>
                    <li><a href="{{ asset('') }}bootstrap/siimple/#">Deep Drop Down 5</a></li>
                  </ul>
                </li>
                <li><a href="{{ asset('') }}bootstrap/siimple/#">Drop Down 3</a></li>
                <li><a href="{{ asset('') }}bootstrap/siimple/#">Drop Down 4</a></li>
                <li><a href="{{ asset('') }}bootstrap/siimple/#">Drop Down 5</a></li>
              </ul>
            </li> --}}
            @if (Auth::check())
              <li><a href="{{ route("academy.sso") }}" class="scrollto">Bimago Academy</a></li>
              <li><a id="btn_logout" class="scrollto">Keluar</a></li>
              @else
              <li><a href="{{ route("academy.redirect") }}" class="scrollto">Bimago Academy</a></li>
              <li><a href="{{ route("auth.login") }}" class="scrollto">Masuk/Daftar</a></li>
            @endif
          </ul>
        </nav><!-- .nav-menu -->

      </div>
    </header><!-- End #header -->
  @endif

  @yield('content')

  <!-- ======= Footer ======= -->
  {{-- <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright {{ date("Y")." ".env("APP_NAME") }} - Template by <strong><span>Siimple</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-landing-page/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End #footer --> --}}

  <!-- Vendor JS Files -->
  @toastr_js
  @toastr_render
  {{-- <script type="text/javascript" src="{{ asset('') }}public/bootstrap/joson/js/jquery-3.4.1.min.js"></script> --}}
  <script type="text/javascript" src="{{ asset('') }}public/bootstrap/joson/js/bootstrap.js"></script>
  <script>
    $(document).on("click", "#btn_logout", function(){
      Swal.fire({
      title: 'Apa anda yakin akan keluar?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#DAA520',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.isConfirmed) {
          var route = "{{ route('auth.logout') }}"
          $.get(route, function(value, status){
            window.location.href = "{{ route('auth.login') }}";
          })
        }
      })
    })
  </script>

</body>

</html>