@inject('info_helper', 'App\Http\Helpers\InfoHelper')
<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>Bimago</title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="{{ asset('') }}public/bootstrap/joson/css/bootstrap.css" />
  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,600,700&display=swap" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{{ asset('') }}public/bootstrap/joson/css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="{{ asset('') }}public/bootstrap/joson/css/responsive.css" rel="stylesheet" />
  <link rel="icon" href="{{ asset('') }}public/assets/img/logo.ico">
  <link href="{{ asset('') }}public/css/fa/css/font-awesome.css" rel="stylesheet" />

  @jquery
  @toastr_css
</head>

<style>
  i.fa-footer {
		display: inline-block;
		border-radius: 60px;
		box-shadow: 0px 0px 2px #888;
		border: 1px solid #000;
		padding: 0.5em 0.6em;
		background-color:#000;
	}

	#prim_footer {
		padding:0px 100px
	}

  @media (max-width: 768px) {
		#prim_footer {
			padding:0px
		}
		.title-footer {
			font-size:120% !important
		}
		#div_part_footer1 {
			display: none;
		}
		#div_part_footer1x {
			display: block !important;
		}
		#div_part_footer2 {
			display: none;
		}
		#div_part_footer2x {
			display: block !important;
		}
	}
</style>

<body class="{{ (in_array($menu, ['spp', 'riwayat_spp', 'lokasi'])) ? 'sub_page' : '' }}">
  <div class="hero_area">
    <!-- header section strats -->
    <header class="header_section" style="background-color:#06bab5">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="{{ route('home') }}">
            <h3 style="width:200px;padding:10px">
                <img src="{{ asset('') }}public/assets/img/ikpm-white.png" style="width:40%;height:40%" alt="">
            </h3>
            {{-- <span> college</span> --}}
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
            <ul class="navbar-nav  ml-auto">
              <li class="nav-item {{ ($menu == 'home') ? 'active' : '' }}">
                <a style="font-size:12px;width: 150px;height: 60px;padding-bottom:60px" class="nav-link" href="{{ route('home') }}">Bimago Bandung <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a style="font-size:12px;width: 150px;height: 60px;padding-bottom:60px" class="nav-link" href="{{ route('home') }}#about">Materi & Informasi</a>
              </li>
              <li class="nav-item">
                <a style="font-size:12px;width: 80px;height: 60px;padding-bottom:60px" class="nav-link" href="{{ route('lokasi', 'all') }}">Tempat</a>
              </li>
              @if (Auth::check() && Session::get('is_login'))
                <li class="nav-item" ><a style="font-size:12px;width: 130px;height: 60px;padding-bottom:60px" href="{{ route("academy.sso") }}" class="nav-link">Belajar Online</a></li>
                <li class="nav-item dropdown dropleft {{ (in_array($menu, ['spp', 'riwayat_spp'])) ? 'active' : '' }}" style="">
                    <a style="font-size:12px;width: 130px;height: 60px;padding-bottom:60px" class="dropdown-toggle nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    SPP
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        {{-- <a style="font-size:12px" class="dropdown-item" href="{{ route('spp') }}">Bayar SPP</a> --}}
                        <a style="font-size:12px" class="dropdown-item" href="{{ route('spp.riwayat') }}">Bayar SPP</a>
                        {{-- <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a> --}}
                    </div>
                </li>
                {{-- <li class="nav-item"></li> --}}
              <li class="nav-item">
                  <a style="font-size:12px;width: 150px;height: 60px;padding-bottom:60px" class="nav-link" href="{{ route('auth.logout' ) }}">Keluar</a>
                </li>
              @else
                <li class="nav-item" ><a style="font-size:12px;width: 130px;height: 60px;padding-bottom:60px" href="{{ route("academy.redirect") }}" class="nav-link">Belajar Online</a></li>
                <li class="nav-item {{ ($menu == 'login') ? 'active' : '' }}">
                    <a style="font-size:12px;width: 80px;margin-top:38px;color:#000;background-color:#fff;border-color:#DAA520;border-radius:25px;margin-right:5px" class="btn btn-primary" href="{{ route('auth.login' ) }}">Masuk</a>
                </li>
                <li class="nav-item">
                    <a style="font-size:12px;width: 80px;margin-top:38px;color:#fff;background-color:#DAA520;border-color:#DAA520;border-radius:25px;margin-right:10px" class="btn btn-primary" href="{{ route('auth.register' ) }}">Daftar</a>
                </li>
              @endif
            </ul>
          </div>
        </nav>
      </div>
    </header>
    @if (in_array($menu, ['spp', 'riwayat_spp', 'lokasi']))
        </div>
    @endif
    <!-- end header section -->
    @yield('content')

  <footer class="footer" id="prim_footer" style="background-color:#D6AF40 !important">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row" id="div_part_footer1">
					<div class="col-lg-12 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
              <br>
              <br>
							<div class="logo">
								<h3 class="title-footer" style="color:#000;font-weight:bold">Siapkan calon santri hadapi</h3>
								<h3 class="title-footer" style="color:#000;padding-bottom:5px;font-weight:bold">ujian masuk Gontor!</h3>
								<br>
								<a href="{{ route('auth.register') }}" class="btn mb-2 ml-0" style="background-color:#fff;color:black;border-radius:20px;font-weight:bold">Daftar Sekarang!</a>
								<br>
								<br>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="#">Bimago Bandung</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ route('home') }}#about">Materi & Informasi</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ route('lokasi', 'all') }}">Tempat</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ (Auth::check() && Session::get('is_login')) ? route('academy.sso') : route('academy.redirect') }}">Belajar Online</a>
								<div class="pull-right">
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-twitter"></i></a>
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-facebook"></i></a>
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
            <hr style="height:1px;border:none;color:#333;background-color:#333;">
					</div>
				</div>
				<div class="row text-center" id="div_part_footer1x" style="display:none">
					<div class="col-lg-12 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
              <br>
              <br>
							<div class="logo">
								<h3 class="title-footer" style="color:#000;font-weight:bold">Siapkan calon santri hadapi</h3>
								<h3 class="title-footer" style="color:#000;padding-bottom:5px;font-weight:bold">ujian masuk Gontor!</h3>
								<br>
								<a href="{{ route('auth.register') }}" class="btn mb-2 ml-0" style="background-color:#fff;color:black;border-radius:20px">Daftar Sekarang!</a>
								<br>
								<br>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="#">Bimago Bandung</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ route('home') }}#about">Materi & Informasi</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ route('lokasi', 'all') }}">Tempat</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;margin-top:15px" href="{{ (Auth::check() && Session::get('is_login')) ? route('academy.sso') : route('academy.redirect') }}">Belajar Online</a>
								<br>
								<br>
								<div class="right">
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-facebook" aria-hidden="true"></i></a>
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-twitter"></i></a>
									<a target="_blank" style="color:#fff" href=""><i class="fa-footer fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
            <hr style="height:1px;border:none;color:#333;background-color:#333;">
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row" id="div_part_footer2">
						<div class="col-lg-3">
							<div class="left">
								<img src="{{ asset('') }}public/assets/img/ikpm-black.png" alt="">
								<img src="{{ asset('') }}public/assets/img/bimago-bdg-black.png" alt="">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pull-left">
								<p style="color:#000;font-size:70%">Copyright © <?= date('Y'); ?> santri.bimago.academy. All Rights Reserved.</p>
								<p style="color:#000;font-size:70%">The use of this website requires acceptance of the Terms and Conditions and Privacy Policy.</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="right">
								<b><p style="margin-bottom:1px;color:#000;font-size:70%">Need help?</p></b>
								<p style="padding-top:0px;color:#000;font-size:70%">Call our support</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="right">
								<p style="color:#000;font-size:100%"><i style="background-color:transparent !important" class="fa fa-footer fa-whatsapp"></i> 081290982859</p>
							</div>
						</div>
					</div>
					<div class="row nowrap" id="div_part_footer2x" style="display:none">
						<div class="col-lg-2">
							<div class="text-center">
								<img src="{{ asset('') }}public/assets/img/ikpm-black.png" alt="">
								<img src="{{ asset('') }}public/assets/img/bimago-bdg-black.png" alt="">
							</div>
						</div>
						<br>
						<div class="col-lg-6">
							<div class="pull-left text-center">
								<p style="margin-bottom:-10px;color:#000;font-size:50%">Copyright © <?= date('Y'); ?> santri.bimago.academy. All Rights Reserved.</p><br>
								<p style="color:#000;font-size:50%">The use of this website requires acceptance of the Terms and Conditions and Privacy Policy.</p>
							</div>
						</div>
						<br>
						<br>
						<div class="col-lg-2 text-center">
							<div class="right">
								<b><p style="margin-bottom:-10px;color:#000;font-size:60%">Need help?</p></b><br>
								<p style="padding-top:0px;color:#000;font-size:60%">Call our support</p>
							</div>
						</div>
						<div class="col-lg-2 text-center">
							<div class="right">
								<p style="color:#000;font-size:100%"><i style="background-color:transparent !important" class="fa fa-footer fa-whatsapp"></i> 081290982859</p>
							</div>
						</div>
            <br>
					</div>
				</div>
			</div>
		</div>
	</footer>

  <!-- end info section -->

  <!-- footer section -->
  {{-- <footer class="container-fluid footer_section">
    <p>
      &copy; Bimago {{ date('Y') }} All Rights Reserved - Beautiful Template By
      <a href="https://html.design/">Free Html Templates Distributed By &
        <a href="https://themewagon.com/">Themewagon</a> </a>
    </p>
  
  </footer> --}}

   
  {{-- <footer class="container-fluid footer_section">
  
  </footer> --}}
 
  <!-- footer section -->
  @toastr_js
  @toastr_render
  <script type="text/javascript" src="{{ asset('') }}public/bootstrap/joson/js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="{{ asset('') }}public/bootstrap/joson/js/bootstrap.js"></script>

</body>

</html>