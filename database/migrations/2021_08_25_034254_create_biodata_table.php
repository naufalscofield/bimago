<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_user');
            $table->string('birthplace');
            $table->date('birthdate');
            $table->enum('graduated', ['SD/MI', 'SMP/MTS', 'SMA/MA']);
            $table->string('school');
            $table->string('parent_name');
            $table->string('parent_phone');
            $table->integer('id_tutor_type');
            $table->integer('regional');
            $table->string('kecamatan');
            $table->longText('address');
            $table->longText('address');
            $table->integer('id_learn_place');
            $table->string('request_day');
            $table->integer('request_time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}
